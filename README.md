# How to build project #

### Requirements ###

* OSX 10.11
* Xcode 7.1.1
* Node.js >=5.0.0 ([Download](https://nodejs.org/en/))
* Facebook SDK

### Installing Facebook SDK ###

* Download SDK from https://developers.facebook.com/docs/ios
* Move contents of folder to ~/Documents/FacebookSDK (sample path ~/Documents/FacebookSDK/FBSDKShareKit.framework)

### Building ###

* Clone from repo
```bash
$ git clone git@bitbucket.org:kstmaks/plysh.git
$ cd plysh
$ git submodule init
$ git submodule update --init --recursive
```
* Install dependencies
```bash
$ npm install
```
* Open XCode project
```bash
$ open ios/plysh.xcodeproj/
```
* Press "Build"

### Run on device (will be changed in future) ###

* Open AppDelegate.m in xcode
* Comment out this line
```objective-c
jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios&dev=true"];
```
* Uncomment this one
```objective-c
jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
```
* Rebuild and run on selected device

### Running tests ###

* Install babel-core for react-native dependency
```bash
$ pushd node_modules/react-native
$ npm install babel-core
$ popd
```
* Run tests
```bash
$ npm test
```
* Run with watch
```bash
$ npm test -- --watch
```
