jest.setMock('react-native')

import expect from 'expect.js'
import _ from 'underscore'

import reducer from '../../app/reducers/routes'
import Routes from '../../app/lib/routes'

describe('routes reducer', () => {
  const splashRoute = _.extend({}, Routes.SPLASH_SCREEN, { index: 0 })
  const introRoute = _.extend({}, Routes.INTRO, { index: 1 })

  const singleRoute = { 
    currentRoute: splashRoute, 
    allRoutes: [splashRoute],
    opIndex: 0,
    result: {},
    modal: false,
    params: {}
  }

  const fullRoute = {
    currentRoute: introRoute,
    allRoutes: [introRoute, splashRoute],
    opIndex: 0,
    result: {},
    modal: false,
    params: {}
  }

  const singleRouteModal = {
    currentRoute: splashRoute, 
    allRoutes: [splashRoute],
    opIndex: 0,
    result: {some: 'result'},
    modal: true,
    params: {}
  }

  const fullRouteModal = {
    currentRoute: introRoute,
    allRoutes: [introRoute, splashRoute],
    opIndex: 0,
    result: {some: 'result'},
    modal: true,
    params: {}
  }

  it('should initialize with splash screen', () => {
    let routeInfo = reducer()
    expect(routeInfo).to.not.have.key('lastAction')
    expect(routeInfo.currentRoute).to.eql(singleRoute.currentRoute)
    expect(routeInfo.allRoutes).to.eql(singleRoute.allRoutes)
    expect(routeInfo.opIndex).to.be(0)
  })

  it('should push new route in front', () => {
    let routeInfo = reducer(reducer(), {
        type: 'ROUTES_PUSH',
        route: Routes.INTRO
    })
    expect(routeInfo.currentRoute).to.eql(fullRoute.currentRoute)
    expect(routeInfo.currentRoute.params).to.eql(fullRoute.currentRoute.params)
    expect(routeInfo.allRoutes).to.eql(fullRoute.allRoutes)
    expect(routeInfo.lastAction).to.be('push')
    expect(routeInfo.opIndex).to.be(1)
  })

  it('should push new route with parameters', () => {
    const params = {findMe: 'here'}
    let routeInfo = reducer(reducer(), {
        type: 'ROUTES_PUSH',
        route: Routes.SPLASH_SCREEN,
        params
    })
    expect(routeInfo.currentRoute.params).to.eql(params)
  })

  it('should push new route with modal', () => {
    let routeInfo = reducer(reducer(), {
        type: 'ROUTES_PUSH_MODAL',
        route: Routes.SPLASH_SCREEN,
    })
    expect(routeInfo.modal).to.be(true)
  })

  it('should save result with push', () => {
    let routeInfo = reducer(singleRouteModal, {
        type: 'ROUTES_PUSH',
        route: Routes.SPLASH_SCREEN,
    })
    expect(routeInfo.modal).to.be(true)
    expect(routeInfo.result).to.eql(singleRouteModal.result)
  })

  it('should pop current route', () => {
    let routeInfo = reducer(fullRoute, {
        type: 'ROUTES_POP'
    })
    expect(routeInfo.currentRoute).to.eql(singleRoute.currentRoute)
    expect(routeInfo.allRoutes).to.eql(singleRoute.allRoutes)
    expect(routeInfo.lastAction).to.be('pop')
    expect(routeInfo.opIndex).to.be(1)
  })

  it('should not pop the only route', () => {
    let routeInfo = reducer(singleRoute, {
        type: 'ROUTES_POP'
    })
    expect(routeInfo.currentRoute).to.eql(singleRoute.currentRoute)
    expect(routeInfo.allRoutes).to.eql(singleRoute.allRoutes)
    expect(routeInfo.lastAction).to.be('pop')
    expect(routeInfo.opIndex).to.be(1)
  })

  it('should pop from modal with result', () => {
    let routeInfo = reducer(fullRouteModal, {
        type: 'ROUTES_POP_MODAL',
        result: {hello: 'world'},
    })
    expect(routeInfo.modal).to.be(false)
    expect(routeInfo.result).to.eql({some: 'result', hello: 'world'})
  })

  it('should pop without result', () => {
    let routeInfo = reducer(fullRouteModal, {
        type: 'ROUTES_POP',
        result: {hello: 'world'},
    })
    expect(routeInfo.modal).to.be(true)
    expect(routeInfo.result).to.eql({some: 'result'})
  })

  it('should save result with pop and modal', () => {
    let routeInfo = reducer(fullRouteModal, {
        type: 'ROUTES_POP',
    })
    expect(routeInfo.modal).to.be(true)
    expect(routeInfo.result).to.eql({some: 'result'})
  })

  it('should not save result with pop and without modal', () => {
    const state = _.extend({}, fullRouteModal, { modal: false })
    let routeInfo = reducer(state, {
        type: 'ROUTES_POP',
    })
    expect(routeInfo.modal).to.be(false)
    expect(routeInfo.result).to.eql({})
  })

  it('should replace current route', () => {
    let splashRoute2 = _.extend({}, Routes.SPLASH_SCREEN, { index: 1, params: {} })

    let routeInfo = reducer(fullRoute, {
        type: 'ROUTES_REPLACE',
        route: Routes.SPLASH_SCREEN
    })
    expect(routeInfo.currentRoute).to.eql(splashRoute2)
    expect(routeInfo.allRoutes).to.eql([splashRoute2, splashRoute])
    expect(routeInfo.lastAction).to.be('replace')
    expect(routeInfo.opIndex).to.be(1)
  })

  it('should replace current route with params', () => {
    const params = {hello: 'there'}

    let routeInfo = reducer(fullRoute, {
        type: 'ROUTES_REPLACE',
        route: Routes.SPLASH_SCREEN,
        params
    })
    expect(routeInfo.currentRoute.params).to.eql(params)
  })

  it('should set params for current route', () => {
    let introRouteModified = _.extend({}, introRoute)
    introRouteModified.params = { sample: 'correct' }

    let routeInfo = reducer(fullRoute, {
        type: 'ROUTES_SET_PARAMS',
        params: { sample: 'correct' }
    })
    expect(routeInfo.currentRoute).to.eql(introRouteModified)
    expect(routeInfo.allRoutes).to.eql([introRouteModified, splashRoute])
    expect(routeInfo.lastAction).to.be('setParams')
    expect(routeInfo.opIndex).to.be(1)
  })

  it('should reset stack', () => {
    const routeInfo = reducer(fullRoute, {
        type: 'ROUTES_RESET_STACK',
        stack: [
          introRoute,
          introRoute,
          splashRoute
        ]
    })

    expect(routeInfo.currentRoute.name).to.eql(introRoute.name)
    expect(routeInfo.currentRoute.index).to.eql(1)
    expect(routeInfo.currentRoute).to.have.key('params')
    expect(routeInfo.allRoutes).to.have.length(3)
  })

  it('should increase opIndex with each operation', () => {
    let routeInfo = reducer()
    expect(routeInfo.opIndex).to.be(0)
    routeInfo = reducer(reducer())
    expect(routeInfo.opIndex).to.be(0)
    routeInfo = reducer(routeInfo, { type: 'ROUTES_PUSH', route: Routes.INTRO })
    expect(routeInfo.opIndex).to.be(1)
    routeInfo = reducer(routeInfo, { type: 'ROUTES_POP' })
    expect(routeInfo.opIndex).to.be(2)
    routeInfo = reducer(routeInfo, { type: 'ROUTES_REPLACE', route: Routes.INTRO })
    expect(routeInfo.opIndex).to.be(3)
    routeInfo = reducer(routeInfo, { type: 'ROUTES_SET_PARAMS', params: {} })
    expect(routeInfo.opIndex).to.be(4)
  })
})
