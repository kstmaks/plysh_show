jest.setMock('react-native')

import expect from 'expect.js'
import collectionReducer from '../../app/reducers/collection'

describe('collection reducer', () => {
  var reducer;
  const elem1 = {test: 1}
  const elem2 = {test: 2}

  beforeEach(() => {
    reducer = collectionReducer('sample')
  })

  it('should add to collection', () => {
    expect(reducer([], {
      type: 'SAMPLE_ADD',
      item: elem1
    })).to.eql([elem1])

    expect(reducer([elem1], {
      type: 'SAMPLE_ADD',
      item: elem2
    })).to.eql([elem1, elem2])
  })

  it('should update collection', () => {
    const newElem = {test: 7}
    expect(reducer([elem1, elem2], {
      type: 'SAMPLE_UPDATE',
      item: newElem,
      selector: x => x.test == 1
    })).to.eql([newElem, elem2])
  })

  it('should remove from collection', () => {
    expect(reducer([elem1, elem2], {
      type: 'SAMPLE_REMOVE',
      selector: x => x.test == 2
    })).to.eql([elem1])
  })

  it('should accept external handlers', () => {
    const reducer = collectionReducer('sample', {
      extendedHandler: (state, action, type) => {
        switch (action.type) {
        case type('ADD_ELEM_2'):
          return state.concat([elem2]);
        default:
          return state;
        }
      }
    })

    expect(reducer([elem1], {
      type: 'SAMPLE_ADD_ELEM_2'
    })).to.eql([elem1, elem2])
  })

  it('should reset collection', () => {
    const reducer = collectionReducer('sample')

    expect(reducer([], {
      type: 'SAMPLE_RESET',
      items: [{name: 'hi'}]
    })).to.eql([{name: 'hi'}])
  })
})
