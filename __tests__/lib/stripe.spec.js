global.fetch = (url, opts) => {
  return {
    then: () => fetch(url, opts),
    catch: () => fetch(url, opts),
    result: {url, opts}
  }
}

import expect from 'expect.js'
import buildStripe from '../../app/lib/stripe'
import base64 from 'base-64'
import formUrlEncoded from 'form-urlencoded'
import querystring from 'querystring'

const stripeSecretKey = 'secretToken'

const stripe = buildStripe(stripeSecretKey)

function assertFetchOptions(call, {method, url, data}) {
  expect(call.url).to.be(url)
  expect(call.opts.method).to.be(method)
  expect(querystring.parse(call.opts.body)).to.eql(querystring.parse(formUrlEncoded.encode(data)))
  expect(call.opts.headers['Accept']).to.be('application/json')
  expect(call.opts.headers['Content-Type']).to.be('application/x-www-form-urlencoded')
  expect(call.opts.headers['Authorization']).to.be('Basic c2VjcmV0VG9rZW46')
}

describe('stripe adapter', () => {
  it('should create and remove customer', () => {
    const call = stripe.createCustomer({
      number: '4242424242424242',
      cvc: '123',
      expMonth: '11',
      expYear: '2017',
      currency: 'usd'
    }, {
      description: '123'
    }).result

    assertFetchOptions(call, {
        url: 'https://api.stripe.com/v1/customers',
        method: 'post',
        data: {
          description: '123',
          source: {
            number: '4242424242424242',
            cvc: '123',
            exp_month: '11',
            exp_year: '2017',
            currency: 'usd',
            object: 'card'
          }
        }
    })
  })

  it('should delete customer', () => {
    const call = stripe.deleteCustomer({
      id: 'someCustomer'
    }, {
      description: '123'
    }).result

    assertFetchOptions(call, {
        url: 'https://api.stripe.com/v1/customers/someCustomer',
        method: 'delete',
        data: {description: '123'}
    })
  })

  it('should create charge', () => {
    const call = stripe.createCharge({
      amount: 400,
      currency: 'usd',
      source: 'customerToken'
    }, {
      description: '123'
    }).result

    assertFetchOptions(call, {
        url: 'https://api.stripe.com/v1/charges',
        method: 'post',
        data: {
          description: '123',
          amount: 400,
          currency: 'usd',
          source: 'customerToken'
        }
    })
  })
})
