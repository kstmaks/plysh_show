import expect from 'expect.js'
import connector from '../app/connector'

jest.setMock('react-redux/native', { 
    connect: (mapState, mapDispatch) => {
      return { mapState, mapDispatch }
    }
})

describe('connector', () => {
  it('should create mapStateToProps and mapDispatchToProps based on actions', () => {
    let { mapState, mapDispatch } = connector({
        tasks: (dispatch => () => dispatch('dispatchTasks')),
        sample: (dispatch => () => dispatch('sampleTasks'))
    })

    var dispatchResults = []
    let dispatch = val => dispatchResults.push(val)

    let mapStateRun = mapState({ tasks: 1 })
    let mapDispatchRun = mapDispatch(dispatch)

    expect(mapDispatchRun).to.have.keys(['tasksActions', 'sampleActions'])
    expect(mapStateRun).to.have.keys(['tasks', 'sample'])
    expect(mapStateRun.tasks).to.be(1)

    mapDispatchRun.tasksActions()
    mapDispatchRun.sampleActions()

    expect(dispatchResults).to.be.eql(['dispatchTasks', 'sampleTasks'])
  })
})
