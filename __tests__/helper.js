import _ from 'underscore'

function mockFetch(mocks) {
  function wrap(result) {
    return {
      text: () => new Promise(r => r(JSON.stringify(result))),
      json: () => new Promise(r => r(result))
    }
  }

  return ((url, params) => {
    const method = params.method
    const mock = _(mocks).findWhere({method, url}) || {}
    return new Promise((resolve, reject) => {
      if (mock.result) {
        resolve(wrap(mock.result))
      }
    })
  })
}

export default {mockFetch}
