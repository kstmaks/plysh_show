import keyboardMixin from './keyboard'

export default function keyboardOffsetMixin(ref) {
  return (Component) => {
    const KBComponent = keyboardMixin(Component)

    KBComponent.prototype.keyboardDidShow = function({end}) {
      if (end && this.refs[ref]) {
        this.refs[ref].measure((fx, fy, width, height, px, py) => {
          const pyh = py + height
          const isCovered = pyh >= end.y && pyh <= (end.y + end.height)

          if (isCovered) {
            this.setState({keyboardOffsetY: end.y - pyh})
          }
        })
      }
    }

    KBComponent.prototype.keyboardWillHide = function({end}) {
      this.setState({keyboardOffsetY: 0})
    }

    KBComponent.prototype.keyboardTranslateStyle = function() {
      return {
        transform: [
          {translateY: (this.state.keyboardOffsetY || 0)}
        ]
      }
    }

    return KBComponent
  }
}
