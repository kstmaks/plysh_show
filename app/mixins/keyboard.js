import _ from 'underscore'
import KeyboardEvents, {Emitter} from 'react-native-keyboardevents'

export default function keyboardMixin(component) {
  const willMount = component.prototype.componentWillMount;
  const willUnmount = component.prototype.componentWillUnmount;
  const events = ['WillShow', 'DidShow', 'WillHide', 'DidHide', 'WillChangeFrame', 'DidChangeFrame']
  const prefix = '__keyboardHadle'

  component.prototype.componentWillMount = function() {
    for (i in events) {
      const event = events[i]
      const method = `keyboard${event}`
      if (method in this) {
        this[`${prefix}${event}`] = _(this[method]).bind(this)
        Emitter.on(KeyboardEvents[`Keyboard${event}Event`], this[`${prefix}${event}`])
      }
    }

    return (willMount && willMount.apply(this, arguments))
  }

  component.prototype.componentWillUnmount = function() {
    for (i in events) {
      const event = events[i]
      Emitter.off(KeyboardEvents[`Keyboard${event}Event`], this[`${prefix}${event}`])
    }

    return (willUnmount && willUnmount.apply(this, arguments))
  }

  return component;
}
