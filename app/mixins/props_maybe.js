import maybe from '../lib/maybe'

export default function(component) {
  component.prototype.mprops = function() {
    return maybe(this.props)
  }

  component.prototype.mstate = function() {
    return maybe(this.state)
  }
}
