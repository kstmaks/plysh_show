import React, { 
  View, 
  Text,
  TouchableHighlight,
  Navigator,
  NavigationBarRouteMapper
} from 'react-native'

import style from './stylesheets/main'
import connector from './connector'
import tasks from './actions/tasks'

import StateNavigator from './components/state_navigator'
import OrientationManager from './components/orientation_manager'

export default class App extends React.Component {
  render() {
    return (
      <OrientationManager>
        <StateNavigator />
      </OrientationManager>
    );
  }
}
