import _ from 'underscore'
import { actionTypeBuilder } from '../actions/collection'


export default function orientationReducer(source, data) {
  const type = actionTypeBuilder('orientation')

  const state = source || {
    orientation: 'UNKNOWN',
    width: 0,
    height: 0
  }

  switch (data.type) {
  case type('SET'): 
    return {
      orientation: data.orientation,
      width: data.width,
      height: data.height
    }
  default: 
    return state
  }
}
