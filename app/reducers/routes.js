import _ from 'underscore'

import { actionTypeBuilder } from '../actions/collection'
import Routes from '../lib/routes'

function getInitialRoute() {
  return _.extend({}, Routes.SPLASH_SCREEN, {index: 0})
}

function getInitialRoutes() {
  let first = getInitialRoute()
  return {
    currentRoute: first,
    allRoutes: [first],
    opIndex: 0
  }
}

export default function routesReducer(target=null, action={}) {
  const type = actionTypeBuilder('routes')
  const state = target == null ? getInitialRoutes() : target
  const opIndex = state.opIndex + 1

  function buildRoute(route, {index, params}) {
    return _.extend({}, route, {
        index: (index || route.index || 0),
        params: (params || route.params || {})
    })
  }

  const modalResult = state.modal ? state.result : {}

  switch (action.type) {
  case type('PUSH'):
    var route = buildRoute(action.route, {
        index: state.currentRoute.index + 1,
        params: action.params
    })
    return {
      currentRoute: route,
      lastAction: 'push',
      allRoutes: [route].concat(state.allRoutes),
      modal: state.modal,
      result: modalResult,
      opIndex
    }

  case type('PUSH_MODAL'):
    var route = buildRoute(action.route, {
        index: state.currentRoute.index + 1,
        params: action.params
    })
    return {
      currentRoute: route,
      lastAction: 'push',
      allRoutes: [route].concat(state.allRoutes),
      modal: true,
      result: state.result,
      opIndex
    }

  case type('POP'):
    if (state.allRoutes.length > 1) {
      let routes = state.allRoutes.slice(1)
      return {
        currentRoute: routes[0],
        lastAction: 'pop',
        allRoutes: routes,
        modal: state.modal,
        result: modalResult,
        opIndex
      }
    } else {
      return {
        currentRoute: state.currentRoute,
        lastAction: 'pop',
        allRoutes: state.allRoutes,
        modal: state.modal,
        result: modalResult,
        opIndex
      }
    }

  case type('POP_MODAL'):
    const newState = routesReducer(state, {type: type('POP')})
    const result = _.extend({}, state.result, action.result)
    return _.extend({}, newState, {modal: false, result})

  case type('REPLACE'):
    var routes = state.allRoutes
    var route = buildRoute(action.route, { 
        index: routes[0].index,
        params: action.params
    })
    let restRoutes = routes.slice(1)
    return {
      currentRoute: route,
      lastAction: 'replace',
      allRoutes: [route].concat(restRoutes),
      modal: false,
      result: {},
      opIndex
    }

  case type('REPLACE_PREVIOUS_AND_POP'):
    var routes = state.allRoutes
    if (routes.length == 1) return state
    var route = routes[1]
    var newRoute = buildRoute(action.route, {index: route.index})
    var newRoutes = [newRoute].concat(routes.slice(2))
    return {
      currentRoute: newRoutes[0],
      lastAction: 'replacePreviousAndPop',
      allRoutes: newRoutes,
      modal: state.modal,
      result: modalResult,
      opIndex
    }
    break

  case type('SET_PARAMS'):
    let route = _.extend({}, state.currentRoute, { params: action.params })
    return {
      currentRoute: route,
      lastAction: 'setParams',
      allRoutes: [route].concat(state.allRoutes.slice(1)),
      modal: state.modal,
      result: state.result,
      opIndex
    }

  case type('RESET_STACK'):
    var routes = action.stack.map(route => buildRoute(route, {}))
    return {
      currentRoute: routes[0],
      lastAction: 'resetStack',
      allRoutes: routes,
      modal: false,
      result: {},
      opIndex
    }

  default:
    return state
  }
}
