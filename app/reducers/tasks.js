import moment from 'moment'
import collectionReducer from './collection'

export default collectionReducer('tasks')
