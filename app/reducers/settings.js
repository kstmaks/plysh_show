import _ from 'underscore'

export default function settingsReducer(state={}, action) {
  switch (action.type) {
  case 'SETTINGS_SET':
    return _.extend({}, state, action.settings)
  default:
    return state
  }
}
