import _ from 'underscore'
import { connect } from 'react-redux/native'

export default function connector(actions={}) {
  const mapState = (state) => {
    return _(actions).chain()
                     .keys()
                     .map(x => [x, state[x]])
                     .object()
                     .value()
  }

  const mapDispatch = (dispatch) => {
    return _(actions).chain()
                     .map((actions, key) => [`${key}Actions`, actions(dispatch)])
                     .object()
                     .value()
  }

  return connect(mapState, mapDispatch)
}
