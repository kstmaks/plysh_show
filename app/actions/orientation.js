import { actionTypeBuilder } from './collection'

export default function orientationActions(dispatch) {
  let type = actionTypeBuilder('orientation')

  return {
    set: ({orientation, width, height}) => {
      dispatch({type: type('SET'), orientation, width, height})
    }
  }
}
