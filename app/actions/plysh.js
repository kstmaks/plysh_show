import moment from 'moment'
import _ from 'underscore'
import persistentCollectionActions from './persistent_collection'
import tasksActionsBuilder from './tasks'
import cardsActionsBuilder from './cards'

export default function plyshActions(dispatch, getState) {

  const tasksActions = tasksActionsBuilder
  const cardsActions = cardsActionsBuilder
  const collActions = persistentCollectionActions('plysh', {
      afterLoad: (item) => {
        item.createdAt = moment(item.createdAt)
        item.task.dateTo = moment(item.task.dateTo)
        return item
      }
  })

  return {
    plysh: (task, image) => dispatch(dispatch => {
      const archivedTask = _.extend({}, task, { archived: true })
      const collection = collActions(dispatch)
      const tasks = tasksActions(dispatch)

      return tasks
        .update(archivedTask)
        .then(() => {
          return collection.add({
              success: true,
              task: archivedTask,
              image: image,
              createdAt: moment()
          })
        })
    }),

    fail: (task) => dispatch(dispatch => {
      const archivedTask = _.extend({}, task, { archived: true })
      const tasks = tasksActions(dispatch)
      const cards = cardsActions(dispatch)
      const collection = collActions(dispatch)

      return tasks
        .update(archivedTask)
        .then(() => {
          return cards.charge({
              amount: task.price.amount,
              currency: task.price.currency,
              stripeCustomerId: task.paymentMethod,
          })
        })
        .then(() => {
          return collection.add({
              success: false,
              task: archivedTask,
              image: archivedTask.image,
              createdAt: moment()
          })
        })
        .catch(err => {
            console.log("ERROR", err)
        })
    }),

    load: () => collActions(dispatch).load()
  }
}
