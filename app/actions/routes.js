import { actionTypeBuilder } from './collection'

export default function routesActions(dispatch) {
  let type = actionTypeBuilder('routes')

  return {
    push: (route, params) => dispatch({ type: type('PUSH'), route, params }),
    pop: () => dispatch({ type: type('POP') }),
    replace: (route, params) => dispatch({ type: type('REPLACE'), route, params }),
    replacePreviousAndPop: (route, params) => dispatch({ type: type('REPLACE_PREVIOUS_AND_POP'), route }),
    setParams: (params) => dispatch({ type: type('SET_PARAMS'), params }),
    resetStack: (stack) => dispatch({ type: type('RESET_STACK'), stack }),

    pushModal: (route, params) => dispatch({ type: type('PUSH_MODAL'), route, params }),
    popModal: (result) => dispatch({ type: type('POP_MODAL'), result })
  }
}
