import { actionTypeBuilder } from './collection'
import persistentCollectionActions from './persistent_collection'
import buildStripe from '../lib/stripe'
import Config from '../config'

const name = 'cards'
const stripe = buildStripe(Config.stripeSecretKey)
const persistentCollection = persistentCollectionActions(name)

export default function cardsActions(dispatch) {
  const type = actionTypeBuilder(name)
  const persistentActions = persistentCollection(dispatch, { id: 'stripeCustomerId' })

  function cutNumber(number) {
    const str = `${number}`
    return '***' + str.substring(str.length - 4, str.length)
  }

  return {
    create: (title, {number, cvc, expMonth, expYear}) => stripe
      .createCustomer({
        number, cvc, expMonth, expYear,
        currency: Config.currency
      })
      .then(resp => {
        const stripeCustomerId = resp.id
        if (stripeCustomerId) {
          return persistentActions.add({
            title, stripeCustomerId, number: cutNumber(number)
          }).then(() => {
            return { success: true }
          })
        } else {
          return Promise.resolve({ success: false, error: resp.error })
        }
      }),

    remove: (card) => persistentActions
      .remove(card)
      .then(() => { return { success: true } }),

    charge: ({amount, currency, stripeCustomerId, description}) => stripe
      .createCharge({
          amount, currency,
          customer: stripeCustomerId
      }, {
        description
      })
      .then(resp => {
          console.log(resp)
      }),

    load: (cards) => persistentActions.load()
  }
}

