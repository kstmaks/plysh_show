import moment from 'moment'
import _ from 'underscore'
import persistentCollectionActions from './persistent_collection'

export default persistentCollectionActions('tasks', {
    autoId: true,

    afterLoad: (item) => {
      return _.extend({}, item, {
          dateTo: moment(item.dateTo),
          archived: !!item.archived
      })
    }
})
