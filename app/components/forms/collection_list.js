import React, { 
  View,
  ListView,
  TouchableHighlight,
  InteractionManager
} from 'react-native'

export default class CollectionList extends React.Component {
  constructor() {
    super()
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  }

  getDataSource() {
    return this.ds.cloneWithRows(this.props.collection)
  }

  _pressRow(rowId) {
    const row = this.props.collection[rowId]
    const handler = this.props.onPress
    InteractionManager.runAfterInteractions(() => {
      handler && handler(row)
    })
  }

  renderTouchable(item, sectionId, rowId) {
    return (
      <TouchableHighlight onPress={() => this._pressRow(rowId)}>
        <View>
          {this.renderPlain(item, sectionId, rowId)}
        </View>
      </TouchableHighlight>
    )
  }

  renderPlain(item, sectionId, rowId) {
    return this.props.renderRow(item, sectionId, rowId)
  }

  renderRow(item, sectionId, rowId) {
    const touchable = !!this.props.onPress
    return touchable ? 
      this.renderTouchable(item, sectionId, rowId) :
      this.renderPlain(item, sectionId, rowId)
  }

  render() {
    return (
      <ListView
        dataSource={this.getDataSource()}
        renderRow={(item, s, r) => this.renderRow(item, s, r)}
        />
    )
  }
}
