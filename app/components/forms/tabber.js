import React, { 
  View, 
  Text,
  TouchableOpacity,
  Dimensions,
  InteractionManager
} from 'react-native'

import ScrollableTabView from 'react-native-scrollable-tab-view'
import _ from 'underscore'

import getValueLinkMixin from '../../mixins/get_value_link'
import styles from '../../stylesheets/main'
import stylesSwiper from '../../stylesheets/swiper'
import Swiper from './swiper'

export default class Tabber extends Swiper {
  selectTab(tab) {
    InteractionManager.runAfterInteractions(() => {
      this.refs.pager.goToPage(tab)
    })
  }

  renderTab(tab, key) {
    const selected = (key == this.state.page)
    const selectedStyle = selected ? styles.tabberTabSelected : {}

    return (
      <TouchableOpacity 
        style={{flex: 1}} 
        onPress={() => this.selectTab(key)} key={key}>
        <Text style={[styles.tabberTab, selectedStyle]}>{tab}</Text>
      </TouchableOpacity>
    )
  }

  renderTabBar() {

    return (
      <View style={styles.tabberBar}>
        {_(this.props.tabs).map((tab, key) => this.renderTab(tab, key))}
      </View>
    )
  }
}
