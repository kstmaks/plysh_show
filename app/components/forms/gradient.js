import React from 'react-native'
import _ from 'underscore'
import LinearGradient from 'react-native-linear-gradient'

export default class Gradient extends React.Component {

  parseHexColor(hex) {
    return parseInt(hex, 16)
  }

  parseRgba(r, g, b, a) {
    return Math.round(b + 0x100 * g + 0x10000 * r + 0x1000000 * a * 0xFF);
  }

  parseColor(color) {
    let hexre = /^\#[0-9a-f]{6,8}$/i
    let rgbre = /^\s*rgb\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*\)\s*$/i
    let rgbare = /^\s*rgba\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9.]+)\s*\)\s*$/i

    let matchRgb = color.match(rgbre);
    let matchRgba = color.match(rgbare);

    switch (true) {
      case !!color.match(hexre):
        let hex = color.substring(1)
        if (hex.length == 8) hex = `FF${hex}`
        return this.parseHexColor(hex)

      case !!matchRgb:
        return this.parseRgba(parseInt(matchRgb[1]), parseInt(matchRgb[2]), parseInt(matchRgb[3]), 1);

      case !!matchRgba:
        return this.parseRgba(
          parseInt(matchRgba[1]), parseInt(matchRgba[2]), parseInt(matchRgba[3]), parseFloat(matchRgba[4]));
    }

    return 0
  }

  parseProps() {
    let colors = this.props.colors.map(this.parseColor.bind(this))
    return _.extend({}, this.props, { colors })
  }

  render() {
    return <LinearGradient {...this.parseProps()} />
  }
}
