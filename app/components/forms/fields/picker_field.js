import React, { 
  View, 
  Text,
  ScrollView,
  TextInput,
  DatePickerIOS,
  PickerIOS,
  PickerItemIOS
} from 'react-native'
import moment from 'moment'
import _ from 'underscore'

import tasksStyles from '../../../stylesheets/tasks'
import LabelValueField from './label_value_field'
import Apply from './apply'
import Field from './field'

export default class PickerField extends React.Component {

  state = { edit: false }

  value() {
    return this.props.value
  }

  label() {
    const option = _(this.props.options).findWhere({value: this.value()})
    return option ? option.label : '---'
  }

  onChange(value) {
    this.props.onChange(value)
  }

  renderEdit() {
    return (
      <Field errors={this.props.errors}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={tasksStyles.detailsRowText}>{this.props.label}</Text>
          <PickerIOS
            style={{alignSelf: 'stretch', height: 200}}
            selectedValue={this.value()}
            onValueChange={(value) => this.onChange(value)}>
            {_(this.props.options).map(option =>
              <PickerItemIOS 
                key={option.key}
                label={option.label}
                value={option.value}
                />
            )}
          </PickerIOS>
          <Apply label="SAVE" onPress={() => this.setState({edit: false})} />
        </View>
      </Field>
    )
  }

  renderRead() {
    return (
      <LabelValueField errors={this.props.errors} label={this.props.label} onPress={() => this.setState({edit: true})}>
        {this.label()}
      </LabelValueField>
    )
  }

  render() {
    if (this.state.edit) {
      return this.renderEdit()
    } else {
      return this.renderRead()
    }
  }
}

