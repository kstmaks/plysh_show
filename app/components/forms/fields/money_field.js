import React, { 
  View, 
  Text,
} from 'react-native'
import _ from 'underscore'

import Money from '../../forms/money'
import TextField from './text_field'
import LabelValueField from './label_value_field'

export default class MoneyField extends React.Component {
  constructor() {
    super()
    this.state = {edit: false, text: ''}
  }

  parseValue(value) {
    const parsed = parseFloat(`${value}`.replace(',', '.'))
    return isNaN(parsed) ? 0 : parsed
  }

  getMoney() {
    return {
      amount: this.parseValue(this.props.value),
      currency: 'USD'
    }
  }

  setEdit(edit) {
    this.state({edit})
  }

  render() {
    if (this.state.edit) {
      return this.renderEdit()
    } else {
      return this.renderRead()
    }
  }

  renderRead() {
    console.log("getMoney()", this.getMoney())
    return (
      <LabelValueField 
        errors={this.props.errors} 
        label={this.props.label} 
        onPress={() => this.setState({edit: true})}
        >
        {(this.props.value) ? <Money money={this.props.value} /> : '---'}
      </LabelValueField>
    )
  }

  onChange(text) {
    this.props.onChange({
        amount: this.parseValue(text),
        currency: this.props.value.currency
    })
  }

  renderEdit() {
    return (
      <TextField 
        {..._(this.props).omit('value', 'onChange')} 
        value={this.parseValue(this.props.value)}
        onChange={(x) => this.onChange(x)}
        autoFocus={true}
        keyboardType="numbers-and-punctuation"
        onBlur={() => this.setState({edit: false})}
        />
    )
  }
}
