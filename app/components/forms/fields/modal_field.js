import React, { 
  View, 
  Text,
  TextInput
} from 'react-native'

import _ from 'underscore'

import Field from './field'
import LabelValueField from './label_value_field'

export default class ModalField extends React.Component {

  checkForUpdate() {
    if (this.props.value != this.props.getValue()) {
      this.props.onChange(this.props.getValue())
    }
  }

  componentDidMount() {
    this.checkForUpdate()
  }

  componentDidUpdate() {
    this.checkForUpdate()
  }

  render() {
    const value = this.props.value

    return (
      <LabelValueField 
        errors={this.props.errors} 
        label={this.props.label} 
        onPress={() => this.props.onRequest()}>
        {this.props.valueLabel || value}
      </LabelValueField>
    )
  }
}
