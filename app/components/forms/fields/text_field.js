import React, { 
  View, 
  Text,
  TextInput
} from 'react-native'

import _ from 'underscore'

import tasksStyles from '../../../stylesheets/tasks'
import keyboardMixin from '../../../mixins/keyboard'
import Field from './field'
import LabelValueField from './label_value_field'

@keyboardMixin
export default class TextField extends React.Component {
  constructor() {
    super()
    this.onChangeDebounce = _.debounce(x => this.onChange(x), 300)
    this.state = {text: '', offsetY: 0, focus: false}
  }

  keyboardDidShow({end}) {
    if (end && this.refs.input) {
      this.refs.input.measure((fx, fy, width, height, px, py) => {
        const isCovered = py >= end.y && py <= (end.y + end.height)

        if (isCovered) {
          this.setState({offsetY: end.y - py - height})
        }
      })
    }
  }
  
  keyboardWillHide({end}) {
    this.setState({offsetY: 0})
  }

  componentDidMount() {
    this.setState({text: this.props.value})
  }

  onChange(e) {
    this.props.onChange(e)
  }

  onChangeText(text) {
    this.setState({text})
    this.onChangeDebounce(text)
  }

  onFocus() {
    this.setState({focus: true})
  }

  onBlur() {
    this.setState({focus: false})
  }

  render() {
    const props = _(this.props).omit('onChange', 'onChangeText', 'value')

    const normalStyles = [tasksStyles.formText]
    const errorStyles = normalStyles.concat([tasksStyles.formTextError])
    const errors = this.props.errors || []
    const style = errors.length > 0 ? errorStyles : normalStyles

    const fieldStyles = (this.state.offsetY >= 0) ? {} : {
      borderTopWidth: 2,
      borderBottomWidth: 2,
      borderColor: 'rgba(0,0,0,.3)',
      marginTop: -19,
      transform: [
        {translateY: this.state.offsetY}
      ]
    }

    const normal = (
      <Field errors={this.props.errors}>
        <TextInput
          ref="input" 
          style={style}
          onChangeText={(e) => this.onChangeText(e)}
          value={this.state.text}
          autoFocus={!!this.props.readable}
          onFocus={() => this.onFocus()}
          onBlur={() => this.onBlur()}
          {...props}
          />
      </Field>
    )

    const readOnly = (
      <LabelValueField 
        errors={this.props.errors} 
        label={this.props.placeholder} 
        onPress={() => this.setState({focus: true})}>
        {this.state.text}
      </LabelValueField>
    )

    return (
      <View style={fieldStyles}>
        {(!this.props.readable || this.state.focus) ? normal : readOnly}
      </View>
    )
  }
}
