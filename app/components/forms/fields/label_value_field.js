import React, { 
  View, 
  Text,
  TouchableOpacity
} from 'react-native'

import tasksStyles from '../../../stylesheets/tasks'
import Field from './field'

export default class LabelValueField extends React.Component {
  render() {
    const label = this.props.label
    const value = this.props.children

    const normalStyles = [tasksStyles.detailsRowText]
    const errorStyles = normalStyles.concat([tasksStyles.detailsRowTextError])
    const errors = this.props.errors || []
    const style = errors.length > 0 ? errorStyles : normalStyles

    return (
      <TouchableOpacity onPress={() => this.props.onPress()}>
        <Field errors={errors}>
          <Text style={style}>{label}</Text>
          <Text style={style.concat([tasksStyles.detailsRowTextRight])}>{value}</Text>
        </Field>
      </TouchableOpacity>
    )
  }
}
