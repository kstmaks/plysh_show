import React, { 
  View, 
  Text
} from 'react-native'

import tasksStyles from '../../../stylesheets/tasks'

export default class Field extends React.Component {
  renderErrorDescription(err, idx) {
    return (
      <View style={tasksStyles.detailsRowErrorDescription}>
        <Text>{err}</Text>
        <View style={tasksStyles.detailsErrorTriangle} />
      </View>
    )
  }

  render() {
    const normalStyles = [tasksStyles.detailsRow]
    const errorStyles = normalStyles.concat([tasksStyles.detailsRowError])
    const errors = this.props.errors || []
    const style = errors.length > 0 ? errorStyles : normalStyles

    return (
      <View>
        <View style={style}>
          {this.props.children}
        </View>
        {errors.map((err, i) => this.renderErrorDescription(err, i))}
      </View>
    )
  }
}
