import React, { 
  View, 
  Text,
  ScrollView,
  TextInput,
  DatePickerIOS
} from 'react-native'
import moment from 'moment'

import tasksStyles from '../../../stylesheets/tasks'
import LabelValueField from './label_value_field'
import Apply from './apply'
import Field from './field'

export default class DateField extends React.Component {

  static formatMoment(value, mode) {
    switch (mode) {
    case 'date': return value.format('MMM DD, YYYY')
    case 'time': return value.format('hh:mm A')
    default: return value.format('hh:mm A, MMM DD, YYYY')
    }
  }

  constructor() {
    super()
    this.state = {edit: false}
  }

  mode() {
    return this.props.mode || 'date'
  }

  formattedValue() {
    const value = this.value()
    return value ? DateField.formatMoment(value, this.mode()) : '---'
  }

  value() {
    return this.props.value
  }

  onChange(date) {
    this.props.onChange(moment(date))
  }

  renderEdit() {
    return (
      <Field errors={this.props.errors}>
        <View style={{flux: 1, alignItems: 'center'}}>
          <Text style={tasksStyles.detailsRowText}>{this.props.label}</Text>
          <DatePickerIOS 
            date={(this.value() || moment()).toDate()} 
            onDateChange={(d) => this.onChange(d)}
            mode={this.mode()}
            />
          <Apply label="SAVE" onPress={() => this.setState({edit: false})} />
        </View>
      </Field>
    )
  }

  renderRead() {
    return (
      <LabelValueField errors={this.props.errors} label={this.props.label} onPress={() => this.setState({edit: true})}>
        {this.formattedValue()}
      </LabelValueField>
    )
  }

  render() {
    if (this.state.edit) {
      return this.renderEdit()
    } else {
      return this.renderRead()
    }
  }
}

