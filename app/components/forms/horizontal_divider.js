import React from 'react-native'

import Gradient from '../forms/gradient'

export default class HorizontalDivider extends React.Component {
  render() {
    return (
      <Gradient
        start={[0.0, 0.0]} end={[1.0, 0.0]}
        locations={[0, 0.15, 0.85, 1.0]}
        colors={['rgba(255,255,255,0)', '#FFECECEC', '#FFECECEC', 'rgba(255,255,255,0)']} 
        style={{height: 1, marginTop: 15, marginBottom: 15}} 
        />
    )
  }
}

