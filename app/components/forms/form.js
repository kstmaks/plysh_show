import React, { 
  View, 
  Text,
  ScrollView,
  InteractionManager
} from 'react-native'
import moment from 'moment'
import _ from 'underscore'
import validate from 'validate.js/validate.js'

import Field from './fields/field'
import LabelValueField from './fields/label_value_field'
import TextField from './fields/text_field'
import MoneyField from './fields/money_field'
import DateField from './fields/date_field'
import ModalField from './fields/modal_field'
import PickerField from './fields/picker_field'

validate.validators.custom = function(value, opts=[]) {
  if (validate.isEmpty(value)) {
    return;
  }

  return _(opts)
    .chain()
    .map(fn => fn(value))
    .filter(x => !!x)
    .flatten()
    .value()
}

validate.validators.money = function(value, opts={}) {
  if (validate.isEmpty(value)) {
    return;
  }

  let errors = []

  const currencies = opts.currencies || ['USD', 'RUB']
  const minValue = 1 || opts.minValue
  const maxValue = opts.maxValue

  if (currencies.indexOf(value.currency) < 0) {
    errors.push('currency is not valid')
  }

  if (value.amount < minValue) {
    errors.push(`amount must at least ${minValue}`)
  } 

  if (maxValue && value.amount > maxValue) {
    errors.push(`amount must less or equal to ${maxValue}`)
  }

  return errors;
}

validate.validators.moment = function(value, opts={}) {
  if (validate.isEmpty(value)) {
    return;
  }

  const unix = value.unix()
  const mode = opts.mode
  let errors = []

  if (opts.earliest && opts.earliest.unix() > unix) {
    const formatted = DateField.formatMoment(opts.earliest, mode)
    errors.push(this.tooEarly || `must be no earlier than ${formatted}`)
  }

  if (opts.latest && opts.latest.unix() < unix) {
    const formatted = DateField.formatMoment(opts.latest, mode)
    errors.push(this.tooLate || `must be no later than ${formatted}`)
  }

  return errors;
}

class Form extends React.Component {
  constructor() {
    super()
    this.state = {errors: {}, loaded: false}
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({loaded: true})
    })
  }

  getValidations() {
    return _(this.getFields()).chain()
      .map((opts, field) => [field, opts.validations || {}])
      .object()
      .value()
  }

  validateFields(newValue) {
    const value = newValue || this.props.value
    const errors = validate(value, this.getValidations()) || {}
    this.setErrors(errors)
    return _(errors).keys().length <= 0
  }

  setErrors(errors) {
    this.setState({errors})
  }

  linkForField(field) {
    return {
      value: this.props.value[field],
      requestChange: (value) => {
        let fieldState = {}
        fieldState[field] = value
        const newState = _.extend({}, this.props.value, fieldState)
        this.props.onChange(newState)
      }
    }
  }

  getField(field, opts, errors) {
    const type = opts.type
    const link = this.linkForField(field)
    const props = _.extend({}, opts, {
        onChange: link.requestChange,
        value: link.value,
        errors
    })

    if (this.state.loaded) {
      switch (type) {
      case 'text': return <TextField {...props}/>
      case 'money': return <MoneyField {...props}/>
      case 'date': return <DateField {...props}/>
      case 'modal': return <ModalField {...props}/>
      case 'picker': return <PickerField {...props}/>
      default: return null
      }
    } else {
      return null
    }
  }

  getFields() {
    return this.props.fields || []
  }

  render() {
    const fields = this.getFields()
    return (
      <ScrollView>
        {_(fields).map((opts, field) => 
          <View key={field}>
            {this.getField(field, opts, this.state.errors[field])}
          </View>
        )}
      </ScrollView>
    )
  }
}

export default Form
