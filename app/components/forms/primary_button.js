import React, { 
  View, 
  Text,
  TouchableOpacity,
  ActivityIndicatorIOS
} from 'react-native'
import _ from 'underscore'

import stylesButton from '../../stylesheets/button'

export default class PrimaryButton extends React.Component {
  render() {
    const style = [stylesButton.primaryButtonWrapper]
    const customStyle = _.flatten([this.props.style] || [])
    const buttonStyle = this.props.type ? [stylesButton[`${this.props.type}Button`]] : []

    const styles = [stylesButton.primaryButton].concat(buttonStyle)

    const touchable = (
      <TouchableOpacity style={styles} onPress={() => this.props.onPress()}>
        <Text style={stylesButton.primaryButtonText}>{this.props.label}</Text>
      </TouchableOpacity>
    )

    const loading = (
      <View style={[styles, {flex: 1, justifyContent: 'center'}]}>
        <ActivityIndicatorIOS
          size="small"
          animating={true}
          style={{flex: 1, backgroundColor: 'rgba(255,255,255,.8)'}} />
      </View>
    )

    return (
      <View style={style.concat(customStyle)}>
        {this.props.loading ? loading : touchable}
      </View>
    )
  }
}
