import React, { 
  View, 
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBarIOS
} from 'react-native'

import ScrollableTabView from 'react-native-scrollable-tab-view';

import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import Routes from '../../lib/routes'
import stylesIntro from '../../stylesheets/intro'
import PrimaryButton from '../forms/primary_button'
import Swiper from '../forms/swiper'

@connector({routes, settings})
export default class Intro extends React.Component {
  componentWillMount() {
    StatusBarIOS.setStyle('default', true)
    this.props.settingsActions.set('finishedIntro', true)
  }
  
  isLastPage() {
    return this.page() == this.getTexts().length - 1
  }

  nextPage() {
    if (this.isLastPage()) {
      this.navigateFrom()
    } else {
      this.changePage(this.page() + 1)
    }
  }

  navigateFrom() {
    this.props.routesActions.replace(Routes.LOGIN)
  }

  renderButtonLabel() {
    return this.isLastPage() ? 'Got It!' : 'Next'
  }

  page(props) {
    const pr = props || this.props
    const num = pr.routes.currentRoute.params.page || 0
    return num
  }

  changePage(page) {
    this.props.routesActions.setParams({page})
  }

  componentDidUpdate(props) {
    if (this.page() != this.page(props)) {
      this.refs.swiper.goToPage(this.page())
    }
  }

  getTexts() {
    return [
      `We're a simple "To Do" List app
      that allows you to put money on
      tasks to help you get things done`,

      `It's proven that people respond
      better to getting things done when
      they have a financial commitment.`,

      `If you complete your task on time,
      you keep your money. If you don't,
      we send it to a charity/celebirty of,
      your choice. It's that simple.`,
    ]
  }

  renderPages() {
    return this.getTexts().map((text, i) =>
      <View style={stylesIntro.slide} key={i}>
        {text.split('\n').map((line, j) =>
          <Text style={stylesIntro.text} key={j}>
            {line.replace(/^\s*(.*)\s*$/, '$1')}
          </Text>
        )}
      </View>
    )
  }

  render() {
    return (
      <View style={{flex:1, backgroundColor: 'white', overflow: 'hidden'}}>
        <Swiper 
          ref="swiper"
          tabs={this.getTexts()}
          onPageChange={x => this.changePage(x)}
          >
          {this.renderPages()}
        </Swiper>
        <PrimaryButton 
          label={this.renderButtonLabel()}
          onPress={() => this.nextPage()}
          />
      </View>
    )
  }
}

