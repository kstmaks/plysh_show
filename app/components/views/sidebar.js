import React, { 
  View, 
  Text,
  TouchableOpacity,
  ListView,
  StatusBarIOS,
  InteractionManager
} from 'react-native'

import SideMenu from 'react-native-side-menu'
import { Icon } from 'react-native-icons'
import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import orientation from '../../actions/orientation'
import tasks from '../../actions/tasks'
import styles from '../../stylesheets/main'
import Routes from '../../lib/routes'
import PrimaryButton from '../forms/primary_button'
import Gradient from '../forms/gradient'
import HorizontalDivider from '../forms/horizontal_divider'

class SidebarButton extends React.Component {
  onPress() {
    let handler = this.props.onPress
    handler && handler()
  }

  stylesNormal() {
    return {
      view: { backgroundColor: 'white' },
      icon: { color: '#CCCCCC' },
      text: { color: '#333333' }
    }
  }

  stylesHighlight() {
    return {
      view: { backgroundColor: '#EEEEEE' },
      icon: { color: '#CCCCCC' },
      text: { color: '#333333' }
    }
  }

  render() {
    const highlight = this.props.highlight
    const styles = highlight ? this.stylesHighlight() : this.stylesNormal()

    return (
      <TouchableOpacity onPress={() => this.onPress()}>
        <View style={[{flex:1, flexDirection: 'row', alignItems: 'center', marginTop: 5, marginBottom: 5}, styles.view]}>
          <Icon
            name={this.props.icon}
            size={20}
            color={styles.icon.color}
            style={{width: 30, height: 25, margin: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }} />
          <Text style={[{fontSize: 14}, styles.text]}>{this.props.label}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

@connector({routes, orientation, settings})
export default class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    const ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => !_.isEqual(r1, r2)
    })

    this.state = {
      dataSource: ds.cloneWithRows([
        { type: 'button', icon: 'fontawesome|home', label: 'Tasks Home', route: Routes.TASKS  },
        { type: 'button', icon: 'fontawesome|archive', label: 'Archive', route: Routes.ARCHIVE  },
        { type: 'button', icon: 'fontawesome|calendar', label: 'Calendar', route: Routes.CALENDAR  },
        { type: 'button', icon: 'fontawesome|calculator', label: 'Payments', route: Routes.CARDS },
        { type: 'divider' },
        { type: 'button', icon: 'fontawesome|list', label: 'Terms and Conditions', route: Routes.TERMS },
        { type: 'button', icon: 'fontawesome|gear', label: 'Settings', route: Routes.SETTINGS },
        { type: 'button', icon: 'fontawesome|user', label: 'Edit Profile', route: Routes.LOGIN },
      ])
    }
  }

  renderRowDivider(data) {
    return <HorizontalDivider />
  }

  renderRowButton(data) {
    const route = data.route || {}
    const isCurrent = this.props.routes.currentRoute.name == route.name
    return <SidebarButton
              icon={data.icon}
              label={data.label}
              highlight={isCurrent}
              onPress={() => {
                  if (!isCurrent) {
                    this.props.routesActions.push(route)
                    InteractionManager.runAfterInteractions(() => {
                      this.props.onChange(false)
                    })
                  }
              }}
              />
  }

  renderRow(data) {
    switch (data.type) {
    case 'divider': return this.renderRowDivider(data);
    case 'button': return this.renderRowButton(data);
    default: return null
    }
  }

  renderContent() {
    const menuWidth = this._offset()
    const { width, height } = this.props.orientation
    const padding = width - this._offset()

    return (
      <View style={{flex: 1, backgroundColor: 'white', paddingRight: padding, width, height}}>
        <Text style={{textAlign: 'center', marginTop: 25, fontSize: 17}}>
          {this.props.settings.name}
        </Text>

        <HorizontalDivider />

        <ListView 
          dataSource={this.state.dataSource} 
          renderRow={data => this.renderRow(data)} 
          />
      </View>
    )
  }

  onChange(val) {
    InteractionManager.runAfterInteractions(() => {
      this.props.onChange(val)
    })
  }

  render() {
    return (
      <SideMenu
        ref="menu"
        openMenuOffset={this._offset()}
        menu={this.renderContent()}
        isOpen={this.props.isOpen}
        onChange={this.props.onChange}
        bounceBackOnOverdraw={false}
        >
        {this.props.children}
      </SideMenu>
    )
  }

  _offset() { 
    const { width, height } = this.props.orientation
    return Math.min(width, height) * 2.0 / 3.0
  }
}
