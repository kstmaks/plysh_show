import React, { 
  View, 
  Text,
  ScrollView,
  ActivityIndicatorIOS,
  InteractionManager,
  TouchableOpacity,
  PickerIOS,
  PickerItemIOS
} from 'react-native'
import _ from 'underscore'
import moment from 'moment'
import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import HorizontalDivider from '../forms/horizontal_divider'
import Calendar from 'react-native-calendar'
import styles from '../../stylesheets/main'
import Routes from '../../lib/routes'
import Main from './main'
import Form from '../forms/form'

@connector({routes, settings})
export default class Settings extends React.Component {
  state = {}

  constructor() {
    super()
    this.saveToSettings = _.debounce(this.saveToSettings, 200)
  }

  componentWillMount() {
    this.setState({settings: this.props.settings})
  }

  getFields() {
    return {
      notificationTime: {
        type: 'picker',
        label: 'Notification Time',
        options: [
          { label: '1 month', value: 24 * 30 },
          { label: '10 days', value: 24 * 10 },
          { label: '7 days', value: 24 * 7 },
          { label: '1 day', value: 24 },
          { label: '12 hours', value: 12 },
          { label: '5 hours', value: 5 },
          { label: '1 hour', value: 1 },
        ]
      }
    }
  }

  saveToSettings(settings) {
    this.props.settingsActions.set(settings)
  }

  onChange(settings) {
    this.setState({settings})
    this.saveToSettings(settings)
  }

  render() {
    return (
      <Main title="Settings">
        <View style={[styles.bgLight, styles.mainBox]}>
          <Form
            value={this.state.settings}
            onChange={(settings) => this.onChange(settings)}
            fields={this.getFields()}
            />
        </View>
      </Main>
    )
  }
}
