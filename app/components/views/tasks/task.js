import React, { 
  View, 
  Text,
  Image,
  Animated,
  InteractionManager,
  TouchableOpacity
} from 'react-native'
import moment from 'moment'
import _ from 'underscore'
import { Icon } from 'react-native-icons'

import maybe from '../../../lib/maybe'
import styles from '../../../stylesheets/main'
import tasksStyles from '../../../stylesheets/tasks'
import Money from '../../forms/money'

class CollapsibleView extends React.Component {
  constructor(props) {
    super()
    this.state = {
      height: props.height
    }
  }

  restore() {
    this.setState({height: this.props.height})
  }

  collapseInstant() {
    this.setState({height: 0})
  }

  collapse() {
    const interval = 10
    const duration = this.props.duration || 100
    const step = this.props.height * (interval / duration)

    this.interval = setInterval(() => {
      InteractionManager.runAfterInteractions(() => {
        if (this.state.height > 0) {
          this.setState({height: this.state.height-step})
        } else {
          clearInterval(this.interval)
        }
      })
    }, interval)
  }

  collapseIn(timeout) {
    this.timeout = setTimeout(() => {
      this.collapse()
    }, timeout)
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
    clearInterval(this.interval)
  }

  render() {
    const activeStyle = { height: this.state.height }
    const style = _.flatten([this.props.style].concat(activeStyle))
    return (
      <View style={style}>
        {this.props.children}
      </View>
    )
  }
}

export default class Task extends React.Component {
  state = {
    opacity: new Animated.Value(0),
  }

  renderDateDiff(date) {
    const months = Math.ceil(moment.duration(date.diff(moment())).asMonths())
    const days = Math.ceil(moment.duration(date.diff(moment())).asDays())
    const hours = Math.ceil(moment.duration(date.diff(moment())).asHours())
    const minutes = Math.ceil(moment.duration(date.diff(moment())).asMinutes())
    const seconds = Math.ceil(moment.duration(date.diff(moment())).asSeconds())

    const options = [
      { value: months, metric: 'months' }, 
      { value: days, metric: 'days' },
      { value: hours, metric: 'hours' },
      { value: minutes, metric: 'minutes' },
      { value: seconds, metric: 'seconds' }
    ]

    const optimal = _(options).find(x => Math.abs(x.value) > 1) || _.last(options)

    if (optimal.value < 0) {
      return { value: -optimal.value, metric: `${optimal.metric} ago` }
    } else {
      return optimal
    }
  }

  componentDidMount() {
    if (this.props.reactOnDestroy) {
      this.checkForAnimation()
    }
  }

  componentDidUpdate(props) {
    if (this.props.reactOnDestroy) {
      if (this.props.task._destroy != props.task._destroy) {
        this.checkForAnimation()
      } else {
        this.setAnimatedState()
      }
    }
  }

  setAnimatedState() {
    if (this.props.task._destroy) {
      this.state.opacity.setValue(1)
      this.refs.collapsible.collapseInstant()
    } else {
      this.state.opacity.setValue(0)
      this.refs.collapsible.restore()
    }
  }

  checkForAnimation() {
    if (this.props.task._destroy) {
      InteractionManager.runAfterInteractions(() => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 500
        }).start()
        this.refs.collapsible.collapseIn(500)
      })
    } else {
      this.state.opacity.setValue(0)
      this.refs.collapsible.restore()
    }
  }

  render() {
    const task = this.props.task
    const plysh = this.props.plysh
    const date = this.renderDateDiff(task.dateTo)
    const status = plysh ? plysh.success ? 'success' : 'fail' : 'inprogress'

    const icon = status == 'success' ? 'check' : status == 'fail' ? 'times' : 'clock-o'
    const color = status == 'success' ? '#195' : status == 'fail' ? '#951' : '#959'

    return (
      <CollapsibleView style={tasksStyles.container} height={100} ref='collapsible'>
        <Image style={tasksStyles.image} source={task.image}>
          <View style={tasksStyles.content}>
            <View style={tasksStyles.textHolder}>
              <View style={tasksStyles.titleAndPrice}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Icon 
                    name={`fontawesome|${icon}`}
                    size={20}
                    color={color}
                    style={{width: 20, height: 30, marginRight: 10}}
                    />
                  <Text style={tasksStyles.title}>
                    {task.title}
                  </Text>
                </View>
                <Money style={[tasksStyles.price, styles.textPrimary]} money={task.price} />
              </View>
              {this.props.plyshTime ?
                <View style={tasksStyles.date}>
                  <Text style={tasksStyles.dateValue}>
                    {date.value}
                  </Text>
                  <Text style={tasksStyles.dateMetric}>
                    {date.metric}
                  </Text>
                </View> :
                <View>
                  <Text style={tasksStyles.dateMetric}>
                    {maybe(plysh).dig('createdAt').try('format', 'DD.MM.YYYY')}
                  </Text>
                </View>
              }
            </View>
          </View>
        </Image>
        <Animated.View style={[tasksStyles.cover, {opacity: this.state.opacity}]} />
      </CollapsibleView>
    )
  }
}

