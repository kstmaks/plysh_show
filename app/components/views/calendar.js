import React, { 
  View, 
  Text,
  ScrollView,
  ActivityIndicatorIOS,
  InteractionManager,
  TouchableOpacity
} from 'react-native'
import _ from 'underscore'
import moment from 'moment'
import { Icon } from 'react-native-icons'
import maybe from '../../lib/maybe'

import connector from '../../connector'
import routes from '../../actions/routes'
import tasks from '../../actions/tasks'
import HorizontalDivider from '../forms/horizontal_divider'
import Calendar from 'react-native-calendar'
import styles from '../../stylesheets/main'
import Routes from '../../lib/routes'
import Main from './main'
import propsMaybe from '../../mixins/props_maybe'

@connector({routes, tasks})
@propsMaybe
export default class TasksCalendar extends React.Component {
  constructor(props) {
    super()

    const selectedDate = maybe(props)
      .dig('routes', 'currentRoute', 'params', 'selectedDate')
      .try('format', 'YYYY-MM-DD')
      .getOrDefault(moment().format('YYYY-MM-DD'))

    this.state = { loading: true, selectedDate }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({loading: false})
    })
  }

  collection() {
    return _(this.props.tasks)
      .chain()
      .filter(x => !!x)
      .filter(x => !x.archived)
      .value()
  }

  eventDates() {
    return _(this.collection()).map(x => x.dateTo.format('YYYY-MM-DD'))
  }

  tasks() {
    return _(this.collection()).filter(x => x.dateTo.format('YYYY-MM-DD') == this.state.selectedDate)
  }

  renderTasks() {
    const tasks = this.tasks()
    return (
      <View style={{margin: 15}}>
        {tasks.length == 0 &&
          <Text style={{marginBottom: 20}}>No tasks for that day</Text>}
        {_(tasks).map(task =>
          <TouchableOpacity onPress={() => this.props.routesActions.push(Routes.TASK_DETAILS, {task})}>
            <Text style={{}} key={task.id}>{task.title}</Text>
            <HorizontalDivider />
          </TouchableOpacity>)}
      </View>
    )
  }

  renderAddButton() {
    if (this.props.routes.modal) {
      return (
        <TouchableOpacity onPress={() => {
          this.props.routesActions.popModal({
              date: moment(this.state.selectedDate)
          })
        }}>
          <Icon 
            name='fontawesome|check'
            size={20}
            color='white'
            style={{width: 50, height: 50, marginTop: -15}}
            />
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity onPress={() => this.props.routesActions.push(Routes.TASK_FORM)}>
          <Icon 
            name='fontawesome|plus'
            size={20}
            color='white'
            style={{width: 50, height: 50, marginTop: -15}}
            />
        </TouchableOpacity>
      )
    }
  }

  onDateSelect(date) {
    this.setState({
      selectedDate: moment(date).format('YYYY-MM-DD')
    })
  }

  render() {
    return (
      <Main title="Plysh Calendar" rightButton={this.renderAddButton()}>
        <View style={[styles.mainBox, styles.bgLight]}>
          <ScrollView>
            {this.state.loading &&
              <ActivityIndicatorIOS size='large' style={{marginTop: 20}} />}
            {!this.state.loading &&
              <Calendar 
                scrollEnabled={false}
                showControls={true}
                selectedDate={this.state.selectedDate}
                onDateSelect={x => this.onDateSelect(x)}
                eventDates={this.eventDates()}
                customStyle={{
                    calendarContainer: styles.calendarContainer,
                    selectedDayCircle: styles.calendarSelectedCircle,
                    controlButton: styles.calendarButton,
                    controlButtonText: { color: 'white' },
                    dayCircleFiller: { padding: 20 },
                    dayButton: { borderTopWidth: 0 },
                    calendarHeading: { borderColor: 'rgba(0,0,0,.1)' },
                    title: { paddingTop: 10 }
                }}
                />}
            <HorizontalDivider />
            {this.renderTasks()}
          </ScrollView>
        </View>
      </Main>
    )
  }
}
