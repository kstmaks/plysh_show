import React, { 
  View, 
  Text,
  ListView,
  TouchableOpacity,
  Image,
  ScrollView,
  InteractionManager,
  ActivityIndicatorIOS
} from 'react-native'
import _ from 'underscore'
import maybe from '../../lib/maybe'

import connector from '../../connector'
import Routes from '../../lib/routes'
import routes from '../../actions/routes'
import tasks from '../../actions/tasks'
import recipients from '../../actions/recipients'
import plysh from '../../actions/plysh'
import cards from '../../actions/cards'
import styles from '../../stylesheets/main'
import Main from './main'
import Money from '../forms/money'
import HorizontalDivider from '../forms/horizontal_divider'

@connector({cards, plysh, routes, recipients})
export default class ConfirmFail extends React.Component {
  state = {
    loading: false
  }

  cancel() {
    this.props.routesActions.pop()
  }

  confirm() {
    const task = this.props.routes.currentRoute.params.task

    InteractionManager.runAfterInteractions(() => {
      this.setState({loading: true})
      this.props.plyshActions.fail(task)
        .then(() => this.props.routesActions.replacePreviousAndPop(Routes.PLYSH_HISTORY))
        .catch(() => this.setState({loading: false}))
    })
  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.cardsActions.load()
    })
  }

  touchable(onPress, children) {
    if (this.state.loading) {
      return children
    } else {
      return (
        <TouchableOpacity onPress={onPress}>{children}</TouchableOpacity>
      )
    }
  }

  render() {
    const task = this.props.routes.currentRoute.params.task
    const card = maybe(_(this.props.cards).findWhere({stripeCustomerId: task.paymentMethod}))
    const recipient = _(this.props.recipients).findWhere({id: task.recipient})

    return (
      <Main title={"Confirm Payment"}>
        <ScrollView>
          <Text style={styles.confirmFailTitle}>
            You've failed
            <Text style={styles.confirmFailTitleTask}>{" "}{task.title}</Text>
          </Text>

          <Image style={styles.confirmFailImage} source={task.image} />

          <View style={styles.bill}>
            <Text style={styles.billDescription}>
            You're going to be charged from {card.dig('title').getOrDefault('your card')}.
            </Text>
            <HorizontalDivider />
            <Text style={styles.billDescription}>Your money will be sent to {recipient.title}.</Text>
            <HorizontalDivider />
            <Text style={styles.billTotal}>
              {"Total: "}
              <Money style={styles.billCharge} money={task.price} />
            </Text>
          </View>

          {this.touchable(() => this.confirm(),
            <View style={styles.confirmFailAccept}>
              {!this.state.loading &&
                <Text style={styles.confirmFailAcceptText}>Confirm</Text>}

              {this.state.loading &&
                <ActivityIndicatorIOS
                  size="small"
                  color='white'
                  />}
            </View>
          )}

          {this.touchable(() => this.cancel(),
            <View style={styles.confirmFailReject}>
              <Text style={styles.confirmFailRejectText}>Cancel</Text>
            </View>
          )}
        </ScrollView>
      </Main>
    )
  }
}
