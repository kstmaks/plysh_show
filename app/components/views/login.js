import React, { 
  Text, View,
  DatePickerIOS,
  TextInput,
  StatusBarIOS,
  Animated
} from 'react-native'
import { Icon } from 'react-native-icons'
import moment from 'moment'

import styles from '../../stylesheets/main'
import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import orientation from '../../actions/orientation'
import Routes from '../../lib/routes'
import keyboardOffsetMixin from '../../mixins/keyboard_offset'

import PrimaryButton from '../forms/primary_button'
import Gradient from '../forms/gradient'

@connector({orientation})
class LoginForm extends React.Component {
  render() {
    const width = Math.min(250, this.props.orientation.width)
    return (
      <View style={[styles.flexCenter, {width}]}>
        {this.props.children}
      </View>
    )
  }
}

@connector({routes, settings})
@keyboardOffsetMixin('input')
export default class Login extends React.Component {

  stepsCount = 2

  constructor(props) {
    super()
    const {name, birthdate} = props.settings
    this.state = {
      step: 0,
      name: name || '',
      invalid: false,
      helloAnim1: new Animated.Value(0),
      helloAnim2: new Animated.Value(0),
      birthdate: birthdate ? moment(birthdate).toDate() : new Date(),
    }
  }

  componentDidMount() {
    StatusBarIOS.setStyle('light-content')
  }

  nextStep() {
    const step = this.state.step + 1
    if (this.isValid()) {
      if (step < this.stepsCount) {
        this.setState({step, invalid: false})
      } else {
        if (this.props.routes.allRoutes.length > 1) {
          this.nextRoute()
        } else {
          this.setState({hello: true}, () => {
            Animated.sequence([
                Animated.timing(this.state.helloAnim1, {
                  toValue: 1,
                  duration: 1000
                }),
                Animated.timing(this.state.helloAnim2, {
                  toValue: 1,
                  duration: 1000
                })
            ]).start()
            setTimeout(() => {
              this.nextRoute()
            }, 2500)
          })
        }
      }
    } else {
      this.setState({invalid: true})
    }
  }

  isValid() {
    switch (this.state.step) {
    case 0: return this.state.name.length > 0
    case 1: return moment(this.state.birthdate).isBefore(moment())
    }
  }

  nextRoute() {
    this.props.settingsActions.set('name', this.state.name)
    .then(() => 
      this.props.settingsActions.set('birthdate', this.state.birthdate.toString()))
    .then(() => {
      if (this.props.routes.allRoutes.length > 1) {
        this.props.routesActions.pop()
      } else {
        this.props.routesActions.replace(Routes.TASKS)
      }
    })
  }

  renderName() {
    return (
      <LoginForm>
        <Text style={styles.loginSubtitle}>How should we call you?</Text>
        <Text/>
        <View style={styles.loginInputWrapper}>
          <Icon
            name="fontawesome|user"
            size={20}
            color="rgba(255,255,255,0.3)"
            style={{width: 20, height: 20, marginRight: 5}} />
          <TextInput 
            ref='input'
            style={styles.loginInput} 
            value={this.state.name}
            onChangeText={name => this.setState({name})}
            />
        </View>
        <View style={styles.loginInputBorder} />
        {this.state.invalid &&
          <Text style={styles.loginError}>Please provide name</Text>}
      </LoginForm>
    )
  }

  renderBirthdate() {
    return (
      <View style={styles.flexCenter}>
        <Text style={styles.loginSubtitle}>Your Date of Birth</Text>
        <Text/>
        <View style={styles.loginDateSelector}>
          <DatePickerIOS 
            date={this.state.birthdate} 
            onDateChange={birthdate => this.setState({birthdate})}
            mode='date'
            />
        </View>
        {this.state.invalid &&
          <Text style={styles.loginError}>Date must be in the past</Text>}
      </View>
    )
  }

  renderHello() {
    return (
      <View style={[styles.root, styles.mainBox]}>
        <View style={styles.flexCenter}>
          <Animated.Text style={[{opacity: this.state.helloAnim1}, styles.logoText]}>Hello</Animated.Text>
          <Animated.Text style={[{opacity: this.state.helloAnim2}, styles.logoText]}>{this.state.name}</Animated.Text>
        </View>
      </View>
    ) 
  }

  renderForm() {
    switch (this.state.step) {
    case 0: return this.renderName()
    case 1: return this.renderBirthdate()
    default: return null
    }
  }

  render() {
    const signUp = this.props.routes.allRoutes.length > 1 ? 'Save' : 'Sign Up'

    if (this.state.hello) {
      return this.renderHello()
    }

    return (
      <View style={[styles.root, styles.mainBox, this.keyboardTranslateStyle()]}>
        <View style={styles.flexCenter}>
          <Text style={styles.logoText}>Plysh</Text>
        </View>
        <View style={styles.flexCenter}>
          {this.renderForm()}
        </View>
        <View style={{padding: 20}}>
          <PrimaryButton
            onPress={() => this.nextStep()}
            label={this.state.step == this.stepsCount - 1 ? signUp : 'Next'}
            />
        </View>
      </View>
    )
  }
}
