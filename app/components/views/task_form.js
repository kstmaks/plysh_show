import React, { 
  View, 
  Text,
  ScrollView,
  TextInput,
  InteractionManager
} from 'react-native'
import moment from 'moment'
import {get} from 'koalaesce'
import _ from 'underscore'

import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import cards from '../../actions/cards'
import tasks from '../../actions/tasks'
import recipients from '../../actions/recipients'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import PrimaryButton from '../forms/primary_button'
import Form from '../forms/form'
import propsMaybe from '../../mixins/props_maybe'

@connector({routes, cards, tasks, recipients, settings})
@propsMaybe
export default class TaskForm extends React.Component {
  constructor(props) {
    super()
    this.state = {
      tutorial: !props.settings.finishedTaskFormTutorial,
      task: props.routes.currentRoute.params.task || {
        timeTo: 23,
        dateTo: moment().add(1, 'day'),
        price: { amount: 0, currency: 'USD' }
      }
    }

    this.state.task.dateTo = moment(this.state.task.dateTo)
  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.cardsActions.load()
    })
  }

  componentDidMount() {
    this.props.settingsActions.set('finishedTaskFormTutorial', true)
  }

  getCard() {
    const stripeCustomerId = this.state.task.paymentMethod
    return _(this.props.cards).findWhere({stripeCustomerId})
  }

  getRecipient() {
    const id = this.state.task.recipient
    return _(this.props.recipients).findWhere({id})
  }

  getFields() {
    const hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    return {
      title: {
        type: 'text',
        placeholder: 'Enter Title',
        validations: {
          presence: true
        }
      },
      dateTo: {
        type: 'modal',
        label: 'Select Due Date',
        valueLabel: this.mstate().dig('task', 'dateTo').try('format', 'DD.MM.YYYY'),
        getValue: () => this.mprops().dig('routes', 'result', 'date').getOrDefault(this.state.task.dateTo),
        onRequest: () => this.props.routesActions.pushModal(Routes.CALENDAR, {selectedDate: this.state.task.dateTo}),
        validations: {
          presence: true,
          // moment: {
          //   earliest: moment().subtract(1, 'day'),
          //   mode: 'date'
          // }
        }
      },
      timeTo: {
        type: 'picker',
        label: 'Due Hour',
        validations: {
          presence: true,
          // custom: [
          //   value => {
          //     const now = moment()
          //     const today = now.format("DD.MM.YYYY") == this.state.task.dateTo.format("DD.MM.YYYY")
          //     const hour = parseFloat(now.format("HH")) >= value

          //     if (today && hour) {
          //       return ['must be in future']
          //     }
          //   }
          // ]
        },
        options: 
          _(hours).map(value => ({ value, label: `${value} AM` })).concat(
            _(hours).map(value => ({ value: value + 12, label: `${value} PM` }))
          )
      },
      price: {
        type: 'money',
        placeholder: 'Enter price',
        label: 'Commitment Amount ($)',
        validations: {
          presence: true,
          money: true
        }
      },
      recipient: {
        type: 'modal',
        label: 'Recipient',
        valueLabel: get(this.getRecipient(), 'title'),
        getValue: () => this.mprops().dig('routes', 'result', 'recipient', 'id').getOrDefault(this.state.task.recipient),
        onRequest: () => this.props.routesActions.pushModal(Routes.RECIPIENTS),
        validations: {
          presence: true,
        }
      },
      paymentMethod: {
        type: 'modal',
        label: 'Payment Method',
        valueLabel: get(this.getCard(), 'title'),
        getValue: () => 
          this.mprops()
              .dig('routes', 'result', 'card', 'stripeCustomerId')
              .getOrDefault(this.state.task.paymentMethod),
        onRequest: () => this.props.routesActions.pushModal(Routes.CARDS),
        validations: {
          presence: true,
        }
      }
    }
  }

  save() {
    const valid = this.refs.form.validateFields()
    if (valid) {
      this.commit(this.parse(this.state.task))
    }
  }

  parse(task) {
    const combinedDate = task.dateTo.format('MM/DD/YYYY') + ` ${task.timeTo}:00`
    return {
      title: task.title,
      dateTo: moment(combinedDate, 'MM/DD/YYYY HH:mm'),
      price: task.price,
      paymentMethod: task.paymentMethod,
      recipient: task.recipient,
      image: this.getRecipient().image
    }
  }

  commit(data) {
    this.props.tasksActions
      .add(data)
      .then(() => this.props.routesActions.pop())
  }

  renderTutorial() {
    return (
      <View style={[styles.bgLight, styles.mainBox]}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>1. Create Task</Text>
          <Text />
          <Text>2. Commit $</Text>
          <Text />
          <Text>3. Select Recepient</Text>
          <Text />
          <Text>4. Get shit done :P</Text>
        </View>
        <PrimaryButton
          label='Next'
          onPress={() => this.setState({tutorial: false})}
          />
      </View>
    )
  }

  renderForm() {
    const task = this.state.task
    const title = (task.title || '').length > 0 ? task.title : 'New task'

    return (
      <Main title={title}>
        <View style={[styles.bgLight, styles.mainBox]}>
          <Form 
            ref="form"
            value={this.state.task}
            onChange={(task) => this.setState({task})}
            fields={this.getFields()} 
            />
        </View>
        <View style={tasksStyles.detailsFooter}>
          <PrimaryButton 
            style={tasksStyles.detailsButton} 
            label='Create Plysh' 
            onPress={() => this.save()}
            />
        </View>
      </Main>
    )
  }

  render() {
    if (this.state.tutorial) {
      return this.renderTutorial()
    } else {
      return this.renderForm()
    }
  }
}

