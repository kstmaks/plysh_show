import React, { 
  View, 
  Text,
  ListView,
  InteractionManager
} from 'react-native'
import moment from 'moment'
import _ from 'underscore'
import {get} from 'koalaesce'

import connector from '../../connector'
import routes from '../../actions/routes'
import tasks from '../../actions/tasks'
import plysh from '../../actions/plysh'
import recipients from '../../actions/recipients'
import cards from '../../actions/cards'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import PrimaryButton from '../forms/primary_button'

@connector({cards, recipients, plysh, routes})
export default class TaskDetails extends React.Component {
  constructor() {
    super()
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2})
  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.cardsActions.load()
    })
  }

  getDataSource(task) {
    const pm = task.paymentMethod
    const card = get(_(this.props.cards).findWhere({stripeCustomerId: pm}), 'title')

    const rec = task.recipient
    const recipient = get(_(this.props.recipients).findWhere({id: rec}), 'title')

    return this.ds.cloneWithRows([
      {title: 'Due Date', render: task.dateTo.format('MMM DD, YYYY')},
      {title: 'Due Time', render: task.dateTo.format('hh:mm A')},
      {title: 'Commitment Amount ($)', render: <Money money={task.price}/>},
      {title: 'Recipient', render: recipient},
      {title: 'Payment Method', render: card},
    ])
  }

  renderRow({title, render}) {
    return (
      <View style={tasksStyles.detailsRow}>
        <Text style={tasksStyles.detailsRowText}>{title}</Text>
        <Text style={[tasksStyles.detailsRowText, tasksStyles.detailsRowTextRight]}>{render}</Text>
      </View>
    )
  }

  repeat() {
    InteractionManager.runAfterInteractions(() => {
      const task = this.props.task
      this.props.routesActions.push(Routes.TASK_FORM, {task})
    })
  }

  plysh() {
    InteractionManager.runAfterInteractions(() => {
      const task = this.props.task
      this.props.routesActions.push(Routes.TASK_CAMERA, {task})
    })
  }

  fail() {
    InteractionManager.runAfterInteractions(() => {
      const task = this.props.task
      this.props.routesActions.push(Routes.CONFIRM_FAIL, {task})
    })
  }

  render() {
    const task = this.props.task

    return (
      <Main title={task.title}>
        <View style={[styles.bgLight, styles.mainBox]}>
          <ListView 
            dataSource={this.getDataSource(task)} 
            renderRow={(row) => this.renderRow(row)}
            />
        </View>
        {task.archived &&
          <View style={tasksStyles.detailsFooter}>
            <PrimaryButton 
              onPress={() => this.repeat()} 
              style={tasksStyles.detailsButton} 
              label="Repeat" 
              />
          </View>}
        {!task.archived &&
          <View style={tasksStyles.detailsFooter}>
            <PrimaryButton 
              onPress={() => this.fail()} 
              style={tasksStyles.detailsButton} 
              type="gray" 
              label="Failed" 
              />
            <PrimaryButton 
              onPress={() => this.plysh()} 
              style={tasksStyles.detailsButton} 
              label="Plyshed" 
              />
          </View>}
      </Main>
    )
  }
}

