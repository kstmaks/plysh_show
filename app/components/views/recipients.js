import React, { 
  View, 
  Text,
  Image,
  TouchableOpacity,
  InteractionManager
} from 'react-native'

import _ from 'underscore'
import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import recipients from '../../actions/recipients'
import orientation from '../../actions/orientation'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'

import Routes from '../../lib/routes'
import Main from './main'
import CollectionList from '../forms/collection_list'
import Tabber from '../forms/tabber'

@connector({recipients, routes, orientation})
export default class Recipients extends React.Component {
  constructor() {
    super()
    this.state = {
      recipients: []
    }
  }

  componentDidMount() {
    const recipients = this.props.recipients
    InteractionManager.runAfterInteractions(() => {
      this.setState({recipients})
    })
  }

  onPress(recipient) {
    if (this.props.routes.modal) { 
      this.props.routesActions.popModal({recipient})
    }
  }

  renderRecipient(rec) {
    return (
      <View style={tasksStyles.container}>
        <Image style={tasksStyles.image} source={rec.image}>
          <View style={[tasksStyles.content, styles.recipientContent]}>
            <Text style={styles.recipientText}>{rec.title}</Text>
          </View>
        </Image>
      </View>
    )
  }

  types() {
    return {
      charity: 'Charity',
      celebrity: 'Celebrities'
    }
  }

  recipientsByType(type) {
    return this.state.recipients.filter(x => x.type == type)
  }

  render() {
    return (
      <Main title="Recipients">
        <View style={[styles.mainBox]}>
          <Tabber
            tabs={_.values(this.types())}
            dimensions={this.props.orientation}>
            {_(this.types()).map((title, type) =>
              <View style={styles.mainBox}>
                <CollectionList
                  collection={this.recipientsByType(type)}
                  onPress={rec => this.onPress(rec)}
                  renderRow={rec => this.renderRecipient(rec)}
                  />
                  {this.props.routes.modal &&
                    <Text style={{fontSize: 10, color: 'white', textAlign: 'center', padding: 3}}>
                      Please be advised that in the case of potential task failure Plysh will collect all the proceeds and take your preferences into account upon distributing
                    </Text>}
              </View>
            )}
          </Tabber>
        </View>
      </Main>
    )
  }
}

