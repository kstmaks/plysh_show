import React, {
  View,
  Text,
  ListView,
  InteractionManager,
  TouchableOpacity,
  Image,
  ActivityIndicatorIOS,
  LayoutAnimation,
  Animated
} from 'react-native'
import Camera from 'react-native-camera'
import { Icon } from 'react-native-icons'
import moment from 'moment'
import ExImage from '../../lib/ex_image'

import connector from '../../connector'
import routes from '../../actions/routes'
import orientation from '../../actions/routes'
import plysh from '../../actions/plysh'
import styles from '../../stylesheets/main'
import Main from './main'

import Routes from '../../lib/routes'

@connector({routes, plysh, orientation})
export default class TaskCamera extends React.Component {
  constructor() {
    super()
    this.state = {
      showCover: true,
      image: false,
      frontFacing: false,
      padding: new Animated.Value(20)
    }
    this.onAnimation = this.onAnimation.bind(this)
    this.state.padding.addListener(this.onAnimation)
  }

  componentWillUnmount() {
    this.state.padding.removeListener(this.onAnimation)
    clearInterval(this.timeout)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.timeout = setTimeout(() => {
        this.setState({showCover: false})
      }, 2000)
    })
  }

  onAnimation({value}) {
    if (value == this.animationEnd) {
      this.onAnimationFinished()
    }
  }

  onAnimationFinished() {
    this.props.plyshActions.plysh(this.props.task, this.state.image).then(() => {
      this.props.routesActions.replacePreviousAndPop(Routes.PLYSH_HISTORY)
    })
  }

  proceedWithImage(image) {
    this.setState({image: { uri: image }})
    LayoutAnimation.spring()
    InteractionManager.runAfterInteractions(() => {
      this.animationEnd = Math.round(this.props.orientation.height/2)-20;
      Animated.spring(this.state.padding, {toValue: this.animationEnd}).start()
    })
  }

  takePicture() {
    if (!this.state.image) {
      this.refs.cam.capture((err, data) => {
        if (!err) {
          this.proceedWithImage(data)
        }
      })
    }
  }

  renderButton(onPress, body) {
    return (
      <Animated.View key="button" style={[
        styles.cameraButtonContainer, {
          bottom: this.state.padding,
          left: this.props.orientation.width/2
        }
      ]}>
        <TouchableOpacity {...{onPress}}>
          <View style={styles.cameraButton}>
            {body}
          </View>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  renderCover() {
    return (
      <View style={styles.cameraCover}>
        <View style={styles.mainBox} />
        <View style={styles.cameraCoverText}>
          <Text style={styles.plyshText}>
            Take a picture to validate{'\n'}your Plysh!!!
          </Text>
        </View>
        <View style={styles.mainBox} />
        {this.renderButton(() => this.takePicture(),
          <Icon
            name="fontawesome|camera"
            size={20}
            color="white"
            style={{width: 30, height: 25, margin: 10, marginTop: 10, textAlign: 'center' }} />
        )}
      </View>
    )
  }

  renderLive() {
    return (
      <View style={styles.cameraLive}>
        <View style={styles.mainBox} />
        {this.renderButton(() => this.takePicture(),
          <Icon
            name="fontawesome|camera"
            size={20}
            color="white"
            style={{width: 30, height: 25, margin: 10, marginTop: 10, textAlign: 'center' }} />
        )}
      </View>
    )
  }

  renderBody() {
    return this.state.showCover ? this.renderCover() : this.renderLive()
  }

  renderCamera() {
    return (
      <Camera
        ref='cam'
        captureTarget={Camera.constants.CaptureTarget.cameraRoll}
        style={styles.mainBox}
        type={this.state.frontFacing ? Camera.constants.Type.front : Camera.constants.Type.back}
        >
        {this.renderBody()}
      </Camera>
    )
  }

  renderSpinner() {
    return (
      <View style={{flex:1, justifyContent: 'flex-end'}}>
        <ExImage source={this.state.image} style={{flex: 1}} />
        {this.renderButton(() => this.takePicture(),
          <ActivityIndicatorIOS
            size="small"
            color='white'
            />
        )}
      </View>
    )
  }

  renderSwitchButton() {
    return (
      <TouchableOpacity
        onPress={() => this.setState({frontFacing: !this.state.frontFacing})}
        >
        <Icon
          name={this.state.frontFacing ? 'fontawesome|users' : 'fontawesome|user'}
          size={20}
          color='white'
          style={{width: 50, height: 50, marginTop: -15}}
          />
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={styles.mainBox}>
        <Main
          title={`Validate ${this.props.task.title}`}
          rightButton={this.renderSwitchButton()}
          >
          {this.state.image ? this.renderSpinner() : this.renderCamera()}
        </Main>
      </View>
    )
  }
}
