import React, { 
  View, 
  Text,
  ListView,
  TouchableOpacity,
  TouchableHighlight,
  InteractionManager,
} from 'react-native'
import _ from 'underscore'
import moment from 'moment'
import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import tasks from '../../actions/tasks'
import plysh from '../../actions/plysh'
import cards from '../../actions/cards'
import orientation from '../../actions/orientation'
import settings from '../../actions/settings'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import Sidebar from './sidebar'
import Tabber from '../forms/tabber'
import PlyshHistory from './plysh_history'

import CollectionList from '../forms/collection_list'
import Task from './tasks/task'

@connector({plysh, cards, tasks, routes, orientation, settings})
export default class Tasks extends React.Component {
  onPress(task) {
    if (task) {
      this.props.routesActions.push(Routes.TASK_DETAILS, {task})
    }
  }

  renderTask(task) {
    const plysh = _(this.props.plysh).find(p => p.task.id == task.id)
    return <Task task={task} plysh={plysh} plyshDate={true} />
  }

  collection() {
    return _(this.props.plysh)
      .chain()
      .sortBy(x => -x.createdAt.diff(moment()))
      .map(x => x.task)
      .value()
  }

  renderTasks() {
    return (
      <CollectionList
        collection={this.collection()}
        onPress={task => this.onPress(task)}
        renderRow={task => this.renderTask(task)}
        />
    )
  }

  render() {
    const title = "Archived Plysh"

    return (
      <Main title={title}>
        <View style={[styles.mainBox, tasksStyles.listContainer]}>
          {this.renderTasks()}
        </View>
      </Main>
    )
  }
}
