import React, { 
  View, 
  Text,
  StatusBarIOS
} from 'react-native'

import connector from '../../connector'
import routes from '../../actions/routes'
import settings from '../../actions/settings'
import style from '../../stylesheets/main'
import styleSplash from '../../stylesheets/splash_screen'
import Routes from '../../lib/routes'

@connector({routes, settings})
export default class SplashScreen extends React.Component {
  componentWillMount() {
    StatusBarIOS.setStyle('light-content', true)
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.settingsActions.load().then(() => {
        if (this.props.settings.finishedIntro) {
          this.toTasks()
        } else {
          this.toIntro()
        }
      }).catch(e => {
        this.toTasks()
      })
    }, 3000)
  }

  toTasks() {
    this.props.routesActions.replace(Routes.TASKS)
  }

  toIntro() {
    this.props.routesActions.replace(Routes.INTRO)
  }

  render() {
    return (
      <View style={[style.bgDefault, styleSplash.back]}>
        <Text style={[styleSplash.text]}>
          accom<Text style={style.textPrimary}>Plysh</Text>
        </Text>
        <Text style={[styleSplash.text]}>things!</Text>
      </View>
    )
  }
}

