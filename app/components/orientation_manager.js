import React, { 
  View, 
  Text,
  TouchableHighlight,
  Navigator,
  NavigationBarRouteMapper,
  Dimensions
} from 'react-native'

import _ from 'underscore'
import Orientation from 'react-native-orientation'
import { NativeModules } from 'react-native'

import connector from '../connector'
import orientation from '../actions/orientation'

const {AppFrame} = NativeModules

@connector({orientation})
export default class OrientationManager extends React.Component {
  constructor() {
    super()
    this.orientationHandler = _(this.updateOrientation).bind(this)
  }

  updateOrientation(orientation) {
    AppFrame.getSize((width, height) => {
      this.props.orientationActions.set({
          orientation, width, height
      })
    })
  }

  componentWillMount() {
    Orientation.addOrientationListener(this.orientationHandler)
    this.updateOrientation('UNKNOWN')
  }

  componentWillUnmount() {
    Orientation.removeOrientationHandler(this.orientationHandler)
  }

  render() {
    return this.props.children
  }
}

