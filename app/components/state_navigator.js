import React, { 
  View, 
  Text,
  Navigator,
  TouchableOpacity,
  Animated,
  ScrollView,
  LayoutAnimation,
  Dimensions,
  InteractionManager
} from 'react-native'
import _ from 'underscore'
import Device from 'react-native-device'
import Overlay from 'react-native-overlay'

import connector from '../connector'
import routes from '../actions/routes'
import orientation from '../actions/orientation'
import styles from '../stylesheets/main'
import NavigationBar from 'react-native-navbar'
import Main from './views/main'
import RouteRenderer from './route_renderer'
import Sidebar from './views/sidebar'

import buildStyleInterpolator from 'buildStyleInterpolator'
import {fromTheLeft, fromTheRight, fadeFromTheRight} from '../lib/navigator_interpolators'

class PhoneNavigator extends React.Component {
  constructor() {
    super()
    this.navigatorDidFocusHandler = _(this.navigatorDidFocus).bind(this)
  }

  componentDidMount() {
    this.refs.navigator.navigationContext.addListener('didfocus', this.navigatorDidFocusHandler)
  }

  componentWillUnmount() {
    this.refs.navigator.navigationContext.removeListener('didfocus', this.navigatorDidFocusHandler)
  }

  navigatorDidFocus(e) {
    const route = e.data.route

    // pop
    if (this.props.routes.currentRoute.index > route.index) {
      InteractionManager.runAfterInteractions(() => {
        this.props.routesActions.resetStack(
          this.props.routes.allRoutes.slice(1)
        )
      })
    }
  }

  componentWillReceiveProps(props) {
    const routes = props.routes

    if (this.props.routes.opIndex != routes.opIndex) {
      switch(routes.lastAction) {
      case 'push':
        this.refs.navigator.push(routes.currentRoute)
        break

      case 'pop':
        this.refs.navigator.pop()
        break

      case 'replace':
        this.refs.navigator.replace(routes.currentRoute)
        break

      case 'replacePreviousAndPop':
        this.refs.navigator.replacePrevious(routes.currentRoute)
        InteractionManager.runAfterInteractions(() => {
          this.refs.navigator.pop()
        })
        break
      }
    }
  }

  renderScene(route) {
    return <RouteRenderer route={route} />
  }

  getNavigatorConfig() {
    const {width, height} = this.props.orientation
    const animationInterpolators = {
      into: buildStyleInterpolator(fromTheRight(width, height)),
      out: buildStyleInterpolator(fadeFromTheRight(width, height)),
    }
    const fullDistance = width
    const config = _.extend({}, Navigator.SceneConfigs.FloatFromRight, {
        fullDistance, animationInterpolators
    })
    return config
  }

  render() {
    const routes = this.props.routes

    return (
      <View style={styles.root}>
        <Navigator
          ref="navigator"
          configureScene={route => this.getNavigatorConfig()}
          initialRouteStack={routes.allRoutes}
          renderScene={route => this.renderScene(route)}
          />
      </View>
    )
  }
}

const StateNavigator = PhoneNavigator

export default connector({routes, orientation})(StateNavigator)
