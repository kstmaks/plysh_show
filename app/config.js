import _ from 'underscore'

const ENV = 'development'

const configs = {
  default: {
    stripeSecretKey: '****',
    currency: 'usd',
    linkToApplication: "http://apple.com"
  },

  development: {
  },

  production: {
  }
}

export default _.extend({}, configs.default, configs[ENV])
