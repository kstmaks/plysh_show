import base64 from 'base-64'
import utf8 from 'utf8'
import querystring from 'querystring'
import _ from 'underscore'
import formUrlEncoded from 'form-urlencoded'

const version = 'v1'

export default function buildStripe(stripeSecretKey) {
  const secretKey = base64.encode(utf8.encode(stripeSecretKey + ':'))

  function formatRequestBody(data, httpMethod='post') {
    return {
      method: httpMethod,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${secretKey}`
      },
      body: formUrlEncoded.encode(data)
    }
  }

  function formatUrl(method) {
    return `https://api.stripe.com/${version}/${method}`
  }

  function request(method, httpMethod='post', data={}) {
    return fetch(
      formatUrl(method), 
      formatRequestBody(data, httpMethod)
    ).then(x => x.json())
  }

  return {
    createCustomer: ({number, cvc, expMonth, expYear, currency}, {description}={}) => {
      return request('customers', 'post', {
          description,
          source: {
            object: 'card',
            number, 
            cvc,
            exp_year: expYear,
            exp_month: expMonth,
            currency: currency
          }
      })
    },

    deleteCustomer: ({id}, {description}={}) => {
      return request(`customers/${id}`, 'delete', {description})
    },

    createCharge: ({amount, currency, customer}, {description}={}) => {
      return request('charges', 'post', {
          amount: Math.round(amount * 100),
          currency, customer, description
      })
    }
  }
}
