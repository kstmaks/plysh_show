import _ from 'underscore'

class Maybe {
  static fromValue(value) {
    if (typeof(value) === 'undefined' || value == null) {
      return new Nothing()
    } else {
      return new Just(value)
    }
  }

  dig(...values) {
    return _(values).foldl((acc, x) => acc.fmap(y => y[x]), this)
  }

  try(fn, ...args) {
    return this.fmap(value => {
      if (fn in value) {
        return value[fn].apply(value, args)
      } else {
        return null
      }
    })
  }

  unsafeGet() {
    return this.value
  }
}

class Just extends Maybe {
  constructor(value) {
    super()
    this.value = value
  }

  fmap(fn) { return Maybe.fromValue(fn(this.value)) }
  getOrDefault(def) { return this.value }
}

class Nothing extends Maybe {
  fmap(fn) { return new Nothing() }
  getOrDefault(def) { return def }
}

export default function maybe(value) {
  return Maybe.fromValue(value)
}
