import {AsyncStorage} from 'react-native'
import _ from 'underscore'

export default class PersistentCollection {
  constructor(name, opts={}) {
    this.name = name
    this.afterLoad = opts.afterLoad || (x => x)
    this.beforeSave = opts.beforeSave || (x => x)
  }

  all() {
    return AsyncStorage
      .getItem(this.name)
      .then(this._deserialize.bind(this))
      .then(items => items.map(this.afterLoad))
  }

  reset(items) {
    const toSave = items.map(this.beforeSave)
    return AsyncStorage.setItem(this.name, this._serialize(toSave))
  }

  add(item) {
    return this.all()
      .then(items => items.concat([item]))
      .then(items => this.reset(items))
  }

  update(selector, item) {
    return this.all()
      .then(items =>
        _(items).map(i => {
            if (selector(i)) {
              return item
            } else {
              return i
            }
        })
      )
      .then(items => this.reset(items))
  }

  remove(selector) {
    return this.all()
      .then(items => _(items).filter(i => !selector(i)))
      .then(items => this.reset(items))
  }

  _deserialize(json) {
    return JSON.parse(json || '[]')
  }

  _serialize(obj) {
    return JSON.stringify(obj)
  }
}
