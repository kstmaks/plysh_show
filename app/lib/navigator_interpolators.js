import _ from 'underscore'

function ratio(width, height) {
  return width / height;
}

export function fromTheRight(width, height) {
  return {
    opacity: {
      value: 1.0,
      type: 'constant',
    },
    transformTranslate: {
      from: {x: width, y: 0, z: 0},
      to: {x: 0, y: 0, z: 0},
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height),
    },

    translateX: {
      from: width,
      to: 0,
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height),
    },

    scaleX: {
      value: 1,
      type: 'constant',
    },
    scaleY: {
      value: 1,
      type: 'constant',
    },
  }
}

export function fromTheLeft(width, height) {
  const right = fromTheRight(width)

  return _.extend({}, right, {
    transformTranslate: {
      from: {x: -width, y: 0, z: 0},
      to: {x: 0, y: 0, z: 0},
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height)
    },
    translateX: {
      from: -width,
      to: 0,
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height),
    },
  })
}

export function fadeFromTheRight(width, height) {
  return {
    // Rotate *requires* you to break out each individual component of
    // rotation (x, y, z, w)
    transformTranslate: {
      from: {x: 0, y: 0, z: 0},
      to: {x: -Math.round(width * 0.3), y: 0, z: 0},
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height),
    },
    // Uncomment to try rotation:
    // Quick guide to reasoning about rotations:
    // http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/#Quaternions
    // transformRotateRadians: {
    //   from: {x: 0, y: 0, z: 0, w: 1},
    //   to: {x: 0, y: 0, z: -0.47, w: 0.87},
    //   min: 0,
    //   max: 1,
    //   type: 'linear',
    //   extrapolate: true
    // },
    transformScale: {
      from: {x: 1, y: 1, z: 1},
      to: {x: 0.95, y: 0.95, z: 1},
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true
    },
    opacity: {
      from: 1,
      to: 0.3,
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: false,
      round: 100,
    },
    translateX: {
      from: 0,
      to: -Math.round(width * 0.3),
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true,
      round: ratio(width, height)
    },
    scaleX: {
      from: 1,
      to: 0.95,
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true
    },
    scaleY: {
      from: 1,
      to: 0.95,
      min: 0,
      max: 1,
      type: 'linear',
      extrapolate: true
    },
  }
}
