'use strict';

import React, {
  EdgeInsetsPropType,
  ImageResizeMode,
  ImageStylePropTypes,
  NativeMethodsMixin,
  NativeModules,
  PropTypes,
  ReactNativeViewAttributes,
  StyleSheet,
  StyleSheetPropType,
  flattenStyle,
  invariant,
  merge,
  requireNativeComponent,
  verifyPropTypes,
  warning,
  findNodeHandle,
  InteractionManager,
  View
} from 'react-native'

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource'
import _ from 'underscore'

var ExImage = React.createClass({
  getDefaultProps: function() {
    return {
      loadingBackgroundColor: '#E3E3E3',
      loadingForegroundColor: '#F53341',
    };
  },

  statics: {
    resizeMode: ImageResizeMode,
  },

  /*
  mixins: [NativeMethodsMixin],

  /**
   * `NativeMethodsMixin` will look for this when invoking `setNativeProps`. We
   * make `this` look like an actual native component class.
  viewConfig: {
    uiViewClassName: 'UIView',
    validAttributes: ReactNativeViewAttributes.UIView
  },
   */

  getInitialState: function() {
    return {
      ratio: 1,
    }
  },

  componentDidMount: function() {
    InteractionManager.runAfterInteractions(() => {
      const source = resolveAssetSource(this.props.source)
      handle = findNodeHandle(this);
      NativeModules.PLYImageManager.getSize(handle, ({width, height}) => {
        if (width > 0 && height > 0) {
          this.setState({ratio: height/width, ratioLoaded: true})
        } else {
          this.setState({ratioLoaded: true})
          if (source.uri && source.uri.startsWith("asset-library")) {
            this.timeout = setTimeout(() => { this.componentDidMount() }, 100)
          }
        }
      })
    })
  },

  componentDidUpdate: function(props) {
    if (this.props.source != props.source) {
      this.componentDidMount()
    }
  },

  componentWillUnmount: function() {
    clearTimeout(this.timeout)
  },

  render: function() {
    for (var prop in nativeOnlyProps) {
      if (this.props[prop] !== undefined) {
        console.warn('Prop `' + prop + ' = ' + this.props[prop] + '` should ' +
          'not be set directly on Image.');
      }
    }
    var source = resolveAssetSource(this.props.source) || {};

    var {width, height} = source;
    var style = [{width, height}, styles.base, this.props.style];

    if (this.props.keepWithin) {
      const fromSource = width > 0 && height > 0;
      const ratio = fromSource ? (height/width) : this.state.ratio;
      const opacity = this.state.ratioLoaded ? 1.0 : 0.0;

      const dwidth = this.props.keepWithin.width;
      const dheight = this.props.keepWithin.height;

      const wwidth = dwidth;
      const wheight = wwidth * ratio;

      const hheight = dheight;
      const hwidth = hheight / ratio;

      if (wheight > dheight) {
        style.push({width: hwidth, height: hheight, opacity});
      } else {
        style.push({width: wwidth, height: wheight, opacity});
      }
    }

    if (this.props.keepRatioHeight) {
      style.push({ height: this.props.keepRatioHeight })
      if (width > 0 && height > 0) {
        style.push({
          width: this.props.keepRatioHeight * (width/height)
        })
      } else {
        style.push({
          width: this.props.keepRatioHeight / this.state.ratio
        })
      }
    }
    // invariant(style, 'style must be initialized');

    var isNetwork = source.uri && source.uri.match(/^https?:/);
    // invariant(
    //   !(isNetwork && source.isStatic),
    //   'static image uris cannot start with "http": "' + source.uri + '"'
    // );
    var isStored = !source.isStatic && !isNetwork;
    var RawImage = isNetwork ? RCTExNetworkImage : RCTExStaticImage;

    if (this.props.style && this.props.style.tintColor) {
      warning(RawImage === RCTExStaticImage, 'tintColor style only supported on static images.');
    }
    var resizeMode = this.props.resizeMode || style.resizeMode || 'cover';

    var nativeProps = _.extend({}, this.props, {
      style,
      tintColor: style.tintColor,
      resizeMode: resizeMode,
    });
    if (nativeProps.cacheThumbnail === undefined) {
      nativeProps.cacheThumbnail = false;
    }
    if (isStored) {
      nativeProps.imageInfo = {
        imageTag: source.uri,
        prezSize: {
          width: style.width || 0,
          height: style.height || 0,
        },
        cacheThumbnail: nativeProps.cacheThumbnail,
      }
    } else {
      nativeProps.src = source.uri;
    }
    if (this.props.defaultSource) {
      nativeProps.defaultImageSrc = this.props.defaultSource.uri;
    }

    nativeProps.onExLoadStart = nativeProps.onLoadStart;
    nativeProps.onExLoadProgress = nativeProps.onLoadProgress;
    nativeProps.onExLoadError = nativeProps.onLoadError;
    nativeProps.onExLoaded = nativeProps.onLoaded;
    delete nativeProps.onLoadStart;
    delete nativeProps.onLoadProgress;
    delete nativeProps.onLoadError;
    delete nativeProps.onLoaded;

    return <RawImage ref='image' {...nativeProps} />;
  }
});

var styles = StyleSheet.create({
  base: {
    overflow: 'hidden',
    backgroundColor: '#EFEFEF',
  },
});

var RCTExNetworkImage = requireNativeComponent('RCTExNetworkImage', null);
var RCTExStaticImage = requireNativeComponent('RCTExStaticImage', null);

var nativeOnlyProps = {
  src: true,
  defaultImageSrc: true,
  imageTag: true,
  contentMode: true,
  imageInfo: true,
};
// if (__DEV__) {
//   verifyPropTypes(ExImage, RCTExStaticImage.viewConfig, nativeOnlyProps);
//   verifyPropTypes(ExImage, RCTExNetworkImage.viewConfig, nativeOnlyProps);
// }

ExImage.calculateCacheSize = function(callback) {
  NativeModules.ExNetworkImageManager.calculateCacheSize(callback);
}

ExImage.clearCache = function(callback) {
  NativeModules.ExNetworkImageManager.clearCache(callback);
}

ExImage.clearThumbnailCache = function(callback) {
  NativeModules.RCTExStaticImageManager.clearThumbnailCache(callback);
}

ExImage.getWidth = function(image, callback) {
}

module.exports = ExImage;
