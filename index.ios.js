import React, { AppRegistry, StyleSheet } from 'react-native'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux/native'

import AppState from './app/app_state'
import App from './app/app'

let AppSmart = connect(x => x)(App)

class Plysh extends React.Component {
  render() {
    return (
      <Provider store={AppState}>
        {() => <AppSmart />}
      </Provider>
    );
  }
}

AppRegistry.registerComponent('plysh', () => Plysh);
